//
//  StackBar.swift
//  FrolliniGame
//
//  Created by Aurelio Aliberti on 23/05/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import UIKit


class StackBar {
    
    
    private var items : [UIImage] = []
    
    func removeOnTop(number: Int){
        
        for _ in 1...number{
        items.removeFirst()
        }
    }
    
    
    func count() -> Int {
        return items.count
        }
    
    func insertOnTop(number: Int, image: UIImage){
        
        for _ in 1...number{
        items.insert(image, at: items.count)
        }     
    }
    
    
    
}


