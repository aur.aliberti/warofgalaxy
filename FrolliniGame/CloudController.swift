//
//  CloudController.swift
//  FrolliniGame
//
//  Created by Pietro Paolo Ugliano on 03/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import CloudKit

class CloudController {
    /*
     
     BISOGNA MODIFICARLO PER POTER SALVARE I PROGRESSI DI GIOCO
     BISOGNA ANCHE CREARE UNA CLASSE APPOSITA PER I DATI SALVATI SIA NEL PROGETTO CHE SU ICLOUD
     
    var container : CKContainer
    var publicDB : CKDatabase
    let privateDB : CKDatabase
    
    var noteArray = [Score]() // conterrà tutti i Record recuperati dal Container.
    var delegate : CloudControllerDelegate?
    
    init() {
        container = CKContainer.default()
        publicDB = container.publicCloudDatabase
        privateDB = container.privateCloudDatabase
    }
    
    func saveScore(score: Int64) {
        
        let recordScore = CKRecord(recordType: "NewRecordType")
        
        // al record setto, titolo e testo uguali a quelli del parametro della funzione
        recordScore.setValue(score, forKey: "score")
        
        /*
         salvo nel Database Pubblico (la record zone public) il record appena creato
         la funzione è una clousure e il corpo stampa un messaggio quando finisce di registrare
         */
        privateDB.save(recordScore, completionHandler: { (record, error) -> Void in
            if let error = error {
                // Insert error handling
                print(error)
                return
            }
            // Insert successfully saved record code
            print("Nota salvata con successo")
        })
    }
    
    func recuperaScore() {
        /*
         Il predicate è il modo in cui i dati devono essere recuperati (ad esempio solo quelli che hanno un certo valore)
         // La CKQuery è il tipo di domanda da fare al database. In particolare si sta richiedendo tutti i tipi record di tipo "Nota" con nessun tipo di filtro (dato che predicate non è impostato)
         */
        let predicate = NSPredicate(value: true)
        let query = CKQuery(recordType: "NewRecordType", predicate: predicate)
        
        
        // performQuery manda in esecuzione la query. il metodo è una closure che restituisce results (un array di anyObject) o error in caso di query fallita
        privateDB.perform(query, inZoneWith: nil) { (results, error) -> Void in
            // se l'errore è diverso da nil quindi è stato generato un errore
            if error != nil {
                print(error)
                // vedi l'articolo sul blog per capire cosa succede qui dentro
                DispatchQueue.main.async {
                    self.delegate?.errorUpdating(error: error! as NSError)
                    
                    return
                }
            } else {
                self.noteArray.removeAll(keepingCapacity: true)
                // se non è stato generato un errore allora itera tutti gli oggetti dell'array result
                for record in results! {
                    /*
                     crea una nota trasformando il record in CKRecord dato che è un AnyObject
                     e inserisce la nota all'interno dell'array di note
                     */
                    let nota = Score(record: record as CKRecord, database: self.privateDB)
                    self.noteArray.append(nota)
                }
                print(self.noteArray[0].score)
            }
            
            // vedi l'articolo sul blog per capire cosa succede
            DispatchQueue.main.async {
                self.delegate?.modelUpdated()
                return
            }
        }
    }
    */
    
}

protocol CloudControllerDelegate {
    func errorUpdating(error: NSError)
    func modelUpdated()
}
