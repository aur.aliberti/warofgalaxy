//
//  ViewController.swift
//  FrolliniGame
//
//  Created by Pietro Paolo Ugliano on 03/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit
import GameKit

class ViewController: UIViewController {
    
    var scene : GameScene?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initGameCenter()
        // Do any additional setup after loading the view.
        
        GameCenterHelper.helper.viewController = self
        
    }
    
    func initGameCenter() {
        
        // Check if user is already authenticated in game center
        if GKLocalPlayer.local.isAuthenticated == false {
            
            // Show the Login Prompt for Game Center
            GKLocalPlayer.local.authenticateHandler = {(viewController, error) -> Void in
                if viewController != nil {
                    self.scene!.isPaused = true
                    self.present(viewController!, animated: true, completion: nil)
                    
                    // Add an observer which calls ‚gameCenterStateChanged‘ to handle a changed game center state
                    let notificationCenter = NotificationCenter.default
                    notificationCenter.addObserver(self, selector: Selector(("gameCenterStateChanged")), name:  NSNotification.Name(rawValue: "GKPlayerAuthenticationDidChangeNotificationName"), object: nil)
                }
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
