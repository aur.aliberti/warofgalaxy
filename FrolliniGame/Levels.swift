//
//  Levels.swift
//  FrolliniGame
//
//  Created by PC on 07/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import SpriteKit
var livelloselezionato : Int = 0
var livelloraggiunto : Int = 7
class Levels : SKSpriteNode{


    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if livelloselezionato == 0 {
            livelloselezionato =  Int(self.name!)!
            if livelloselezionato <= (livelloraggiunto) {
                let changeSceneAction = SKAction.run(changeGameOverScene)
                let waitToChangeScene = SKAction.wait(forDuration: 1)
                let changeSceneSequence = SKAction.sequence([waitToChangeScene,changeSceneAction])
                print(livelloselezionato)
                
                self.run(changeSceneSequence)
            }
        }
    }
    
    func changeGameOverScene(){
        
        let sceneToMoveTo = GameScene(size: (scene?.view?.frame.size)!)
        sceneToMoveTo.scaleMode = scene!.scaleMode
        let myTransition = SKTransition.fade(withDuration: 0.5)
        scene?.view!.presentScene(sceneToMoveTo, transition: myTransition)
        
        }
    
    
}


