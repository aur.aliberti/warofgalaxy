//
//  InitialGameScene.swift
//  FrolliniGame
//
//  Created by Pietro Paolo Ugliano on 03/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit
import SpriteKit
import CloudKit
import GameKit

class InitialGameScene: SKScene, CloudControllerDelegate {
    func errorUpdating(error: NSError) {
        let alertController = UIAlertController(
            title: "Errore CloudKit",
            message: error.localizedDescription,
            preferredStyle: UIAlertController.Style.alert
        )
        
        let exit = UIAlertAction(title: "Chiudi", style: UIAlertAction.Style.cancel,
                                 handler: {(paramAction:UIAlertAction!) in
                                    print("[AlertView] Chiusa")
        })
        alertController.addAction(exit)
    }
    
    func modelUpdated() {
        
    }
    
    var cloudCtr = CloudController()

}
