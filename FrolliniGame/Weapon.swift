//
//  Weapon.swift
//  FrolliniGame
//
//  Created by Aurelio Aliberti on 10/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import SpriteKit

public enum WeaponType : Int{
    
    case laser = 1
    case plasma
    case gauss
}

class Weapon : SKSpriteNode {
    
        var weaponType = WeaponType.laser
        var damage = 0
        
        
        override init(texture: SKTexture?, color: UIColor, size: CGSize) {
            
            self.damage = 1
            super.init(texture: texture, color: color, size: size)
        }
        
        
        convenience init(name: String, damage: Int, weaponType: EnemyType) {
            let texture = SKTexture(imageNamed: name)
            self.init(texture: texture, color: SKColor.clear, size: texture.size())
            self.damage = damage
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        
        
        
    }

