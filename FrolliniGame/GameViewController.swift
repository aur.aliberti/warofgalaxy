//
//  GameViewController.swift
//  FrolliniGame
//
//  Created by Aurelio Aliberti on 10/05/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = HomeScene(size: view.bounds.size)
        print("\(view.bounds.size)")
        let skView = view as! SKView
        
        let screenWidth = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        scene.size = CGSize(width: screenWidth, height: screenHeight)
        //mostro a schermo gli FPS
        skView.showsFPS = false
        //mostro il numero di nodi
        skView.showsNodeCount = false
        //ignoro l'ordine di renderizzazione dei nodi
        skView.ignoresSiblingOrder = true
        //setto il modo di rappresentare le dimensioni della scena in modo da non scalarle
        scene.scaleMode = .aspectFill
        //aggiungo alla SKView alla scena
        skView.presentScene(scene)
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    
    
    override var prefersHomeIndicatorAutoHidden: Bool{
        return true
    }
    
    
    
    
    
    
}
            
