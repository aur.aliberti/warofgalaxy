//
//  HomeScene.swift
//  FrolliniGame
//
//  Created by PC on 17/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import SpriteKit

class HomeScene: SKScene {
    
    var gameArea: CGRect
    let background = SKSpriteNode(imageNamed: "backgroundUniverse")
    let backFrame = SKSpriteNode(imageNamed: "FrameMainMenu")
    let logo = SKSpriteNode(imageNamed: "Logo")
    let survivalButton = SKSpriteNode(imageNamed: "SurvivalButton")
    let campaignButton = SKSpriteNode(imageNamed: "CampaignButton")
    let optionButton = SKSpriteNode(imageNamed: "OptionButton")
    
    var bullet = SKSpriteNode()
 
    var backgroundSound = SKAudioNode(fileNamed: "brunduMenu.wav")
    var buttonPressed = SKAction.playSoundFileNamed("buttonPressed", waitForCompletion: false)
    
    let redEnemy = SKSpriteNode(imageNamed: "redEnemy")
    let greenEnemy = SKSpriteNode(imageNamed: "greenEnemy")
    let blueEnemy = SKSpriteNode(imageNamed: "blueEnemy")
    let flame1 = SKSpriteNode(imageNamed: "flame")
    let flame2 = SKSpriteNode(imageNamed: "flame")
    let flame3 = SKSpriteNode(imageNamed: "flame")
    
   
    
    
    override init(size: CGSize) {
        
        gameArea = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        super.init(size: size)
        
        print("Height: \(size.height), Width: \(size.width)")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMove(to view: SKView) {
        
        
        setupEnemy()
        
      
        
        let spawningBullet = SKAction.run(spawnBulletFromRight)
        let waitToSpawn = SKAction.wait(forDuration: 2)
        let spawnSequence = SKAction.sequence([spawningBullet,waitToSpawn])
        self.run(SKAction.repeatForever(spawnSequence))
        
        
        self.addChild(backgroundSound)
        
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.size.height = self.size.height
        background.size.width = self.size.width
        background.zPosition = 1
        self.addChild(background)
        survivalButton.position = CGPoint(x: self.frame.midX, y: 250)
        survivalButton.zPosition = 6
        survivalButton.name = "survivalButton"
        self.addChild(survivalButton)
        campaignButton.position = CGPoint(x: self.frame.midX, y: 170)
        campaignButton.zPosition = 6
        campaignButton.name = "campaignButton"
        self.addChild(campaignButton)
        optionButton.position = CGPoint(x: self.frame.midX, y: 90)
        optionButton.name = "optionButton"
        optionButton.zPosition = 6
        self.addChild(optionButton)
        backFrame.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        backFrame.size.height = self.size.height
        backFrame.size.width = self.size.width
        backFrame.zPosition = 5
        self.addChild(backFrame)
        logo.position = CGPoint(x: self.frame.midX, y: 700)
        logo.zPosition = 4
        self.addChild(logo)
    }
    
    func playSound(sound : SKAction ){
        
        run(sound)
        
    }
    
    func spawnBulletFromRight(){
        
        let bulletType = Int.random(in: 1...3)
        let randomPosition = Int.random(in: 500...800)
        
        if bulletType == 1 {
            
            bullet = SKSpriteNode(texture: SKTexture(imageNamed: "Laser1Left"))
            
        }else if bulletType == 2{
            
            bullet = SKSpriteNode(texture: SKTexture(imageNamed: "PlasmaBulletLeft"))
            
        }else if bulletType == 3{
            
            bullet = SKSpriteNode(texture: SKTexture(imageNamed: "GaussBulletLeft"))
        }
        
        
        bullet.zPosition = 3
        bullet.position = CGPoint(x: 404, y: randomPosition)
        
        self.addChild(bullet)
        let delete = SKAction.removeFromParent()
        let moveLeft = SKAction.move(to: CGPoint(x: -20, y: randomPosition), duration: 3)
        let sequence = SKAction.sequence([moveLeft,delete,SKAction.wait(forDuration: 2)])
        
        bullet.run(sequence)
        
        }
    
    func setupEnemy(){
        
        
        
        redEnemy.position = CGPoint(x: self.frame.width / 2, y: 530)
        greenEnemy.position = CGPoint(x: redEnemy.position.x - 100, y: 430)
        blueEnemy.position = CGPoint(x: redEnemy.position.x + 100, y: 430)
        
  
        redEnemy.zPosition = 7
        redEnemy.setScale(0.3)
        
        greenEnemy.setScale(0.3)
        greenEnemy.zPosition = 7
        
        blueEnemy.setScale(0.3)
        blueEnemy.zPosition = 7
        
        
        self.addChild(redEnemy)
        self.addChild(blueEnemy)
        self.addChild(greenEnemy)
        
        
        let moveUpY = SKAction.moveBy(x: 0, y: 100, duration: 3)
        let moveDownY = SKAction.moveBy(x: 0, y: -100, duration: 3)
        let wait = SKAction.wait(forDuration: 1)
        let movingSequence1 = SKAction.sequence([moveUpY,wait,moveDownY,wait])
        let movingSequence2 = SKAction.sequence([moveDownY,wait,moveUpY,wait])
        
        
       

        let flame1 = SKSpriteNode(imageNamed: "flame")
        let flame2 = SKSpriteNode(imageNamed: "flame")
        let flame3 = SKSpriteNode(imageNamed: "flame")
        
        flame1.zPosition = flame1.zPosition - 2
        flame2.zPosition = flame1.zPosition - 2
        flame3.zPosition = flame3.zPosition - 2
        
        flame1.position.y = flame1.position.y - 20
        flame2.position.y = flame1.position.y - 20
        flame3.position.y = flame1.position.y - 30
        
      
        greenEnemy.addChild(flame1)
        redEnemy.addChild(flame2)
        blueEnemy.addChild(flame3)

        
        let moveFireUp = SKAction.moveBy(x: 0, y: 20, duration: 0.5)
        let moveFireDown = SKAction.moveBy(x: 0, y: -20, duration: 0.5)
        
        let fireSequence = SKAction.sequence([moveFireDown,moveFireUp])
        
        flame1.run(SKAction.repeatForever(fireSequence))
        flame2.run(SKAction.repeatForever(fireSequence))
        flame3.run(SKAction.repeatForever(fireSequence))
        
        
        
        blueEnemy.run(SKAction.repeatForever(movingSequence1))
        greenEnemy.run(SKAction.repeatForever(movingSequence1))
        redEnemy.run(SKAction.repeatForever(movingSequence2))
        
    }
    
    
    let alert = SKSpriteNode(imageNamed: "AllertContentNotAvailable")
    let closeAlert = SKSpriteNode(imageNamed: "Close")
    var isOnScene = false
    
    func presentAlert(){
        
        isOnScene = true
        
        alert.zPosition = 10
        alert.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        alert.alpha = 0
        
        closeAlert.zPosition = 12
        closeAlert.name = "closeAlert"
        closeAlert.position.x = 140
        closeAlert.position.y = 120
        
        self.addChild(alert)
        alert.addChild(closeAlert)
        
        
        let fadeIn = SKAction.fadeIn(withDuration: 1)
      
        
        alert.run(fadeIn)
        closeAlert.run(fadeIn)
        
        }
    
    
    func closingAlert(){
        
        isOnScene = false
        let fadeOut = SKAction.fadeOut(withDuration: 1)
        
        closeAlert.run(fadeOut)
        alert.run(fadeOut)
        
        
        alert.removeAllChildren()
        alert.removeFromParent()
        
        
        
        
    }


    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        for touch in touches {
            
            let location = touch.location(in: self)
            let touchedNode = self.atPoint(location)
            
            
            if touchedNode.name == "survivalButton" {
                
                let moveDown = SKAction.moveBy(x: 0, y: -10, duration: 0.1)
                let moveUp = SKAction.moveBy(x: 0, y: 10, duration: 0.1)
                
                let sequence = SKAction.sequence([moveDown,moveUp])
                touchedNode.run(sequence)
              
                let changeSceneAction = SKAction.run(changeScene)
                let waitToChangeScene = SKAction.wait(forDuration: 0.5)
                let changeSceneSequence = SKAction.sequence([waitToChangeScene,changeSceneAction])
                self.run(changeSceneSequence)
            }
            else if touchedNode.name == "campaignButton" && isOnScene == false{
                
                let moveDown = SKAction.moveBy(x: 0, y: -10, duration: 0.1)
                let moveUp = SKAction.moveBy(x: 0, y: 10, duration: 0.1)
                
               let sequence = SKAction.sequence([moveDown,moveUp])
                touchedNode.run(sequence)
                playSound(sound: buttonPressed)
                presentAlert()
            }
            else if touchedNode.name == "optionButton" && isOnScene == false {
                
                let moveDown = SKAction.moveBy(x: 0, y: -10, duration: 0.1)
                let moveUp = SKAction.moveBy(x: 0, y: 10, duration: 0.1)
                
                let sequence = SKAction.sequence([moveDown,moveUp])
                touchedNode.run(sequence)
                playSound(sound: buttonPressed)
                presentAlert()
         
            }
            else if touchedNode.name == "closeAlert" {
            
            playSound(sound: buttonPressed)
            closingAlert()
            }
            
            
            }
    }





        func changeScene(){
            
            let sceneToMoveTo = GameScene(size: (scene?.view?.frame.size)!)
            sceneToMoveTo.scaleMode = scene!.scaleMode
            scene?.view!.presentScene(sceneToMoveTo)
        }
        




}
