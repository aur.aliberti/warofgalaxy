//
//  Enemy.swift
//  FrolliniGame
//
//  Created by Aurelio Aliberti on 17/05/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import SpriteKit

public enum EnemyType : Int{
    
    case normal = 1
    case medium = 2
    case hard = 3
    case health = 4
    case addOnMana = 5
}

class Enemy: SKSpriteNode {
    
    var enemyType = EnemyType.normal
    var health: Int!
    var enemyBar = [SKSpriteNode]()
    var fire : SKSpriteNode?
    
    override init(texture: SKTexture!, color: SKColor, size: CGSize) {
        self.health = 1 // Some sort of sensible default
        //self.enemyType = EnemyType.normal
        super.init(texture: texture, color: color, size: size)
    }
    
    convenience init(name: String, health: Int, enemyType: EnemyType) {
        let texture = SKTexture(imageNamed: name)
        self.init(texture: texture, color: SKColor.clear, size: texture.size())
        self.health = health
        
        if enemyType == .normal || enemyType == .medium || enemyType == .hard{
            
           
           let fireTexture = SKTexture(imageNamed: "flame")
            fire = SKSpriteNode(texture: fireTexture)
            
        }
        else if enemyType == .health || enemyType == .addOnMana {
            
         
            let fireCutTexture = SKTexture(imageNamed: "flameCut")
            fire = SKSpriteNode(texture: fireCutTexture)
            
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        // Decoding length here would be nice...
        super.init(coder: aDecoder)
    }
    
    func gotHit(_ damage: Int) {
        //self.enemyBar[self.health-1].texture = SKTexture(imageNamed: "whiteBar")
        self.health -= damage
        
    }
    
    func isDead() -> Bool {
        var result: Bool = false
        if self.health <= 0 {
            result = true
        }
        return result
    }
    
   
    
    
    
   func playFireAnimation(){
    
    
   self.fire!.zPosition = self.zPosition - 3
    
    var scaleIn = SKAction.scale(to: 1.3, duration: 0.8)
    var scaleOut = SKAction.scale(to: 1, duration: 0.8)
    
    if self.enemyType == .medium{
        
       self.fire!.position.y = fire!.position.y - 50
        
    }else if self.enemyType == .health || self.enemyType == .addOnMana{
        
     
        scaleIn = SKAction.scale(to: 0.5, duration: 0.8)
        scaleOut = SKAction.scale(to: 0.2, duration: 0.8)
        self.fire!.position.y = fire!.position.y - 80
        
    }
    else{
        
     
        self.fire!.position.y = fire!.position.y - 60
    }
    
    let fireSequence = SKAction.sequence([scaleIn,scaleOut])
    
    fire!.run(SKAction.repeatForever(fireSequence))
    
    self.addChild(fire!)
    
    }
    
    
    func changeTexture(texture: SKTexture){
        
        print("Changing Texture!")
        let fadeOut = SKAction.fadeOut(withDuration: 0.3)
        let fadeIn = SKAction.fadeIn(withDuration: 0.3)
        let wait = SKAction.wait(forDuration: 2)
        
        let changeTexture = SKAction.setTexture(texture)
        
        let changetextureSequence = SKAction.sequence([fadeOut,changeTexture,fadeIn,wait])
        self.run(changetextureSequence)
        
        
    }
    
    
    func moveDown(position: CGPoint) -> Int{
        
        let moveTo = SKAction.move(to: position, duration: 4)
        
        let delete = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([moveTo,delete])
        
        self.run(sequence)
        
        return 1
        
        
    }
    
    
    
    
    
    
    
    
    
    }

