//
//  MenuScene.swift
//  FrolliniGame
//
//  Created by PC on 06/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation

var stagelevel = 1
class MenuScene: SKScene {
    
    var stageImage : String = ""
    var lockImage : String = ""
    var gameArea: CGRect
    var label = SKLabelNode()
    var selectlevel1 = Levels()
    var selectlevel2 = Levels()
    var selectlevel3 = Levels()
    var selectlevel4 = Levels()
    var selectlevel5 = Levels()
    var backgroundLevelSelection = SKSpriteNode(imageNamed: "stage1Background")
   
    override init(size: CGSize) {
        
        gameArea = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        super.init(size: size)
        
        print("Height: \(size.height), Width: \(size.width)")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    override func willMove(from view: SKView) {
        if let gestures = view.gestureRecognizers {
            for gesture in gestures {
                if let recognizer = gesture as? UISwipeGestureRecognizer {
                    view.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    override func didMove(to view: SKView) {
        if stagelevel == 1 {
            stageImage = "moon"
        }
        if stagelevel == 2 {
            stageImage = "mars"
        }
        let swipeRight:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedRight))
        swipeRight.direction = .right
        let swipeLeft:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedLeft(sender:)))
        swipeLeft.direction = .left
        
        
        view.addGestureRecognizer(swipeLeft)
        view.addGestureRecognizer(swipeRight)
        if stagelevel == 1 {
            stageImage = "moon"
            lockImage = "moon.oscure"
        }
        
        if stagelevel == 2 {
            stageImage = "mars"
            lockImage = "marsLocked"
        }
        backgroundLevelSelection = SKSpriteNode(imageNamed: "stage1Background")
        backgroundLevelSelection.position = CGPoint(x: (self.view?.frame.midX)!, y: (self.view?.frame.midY)!)
        backgroundLevelSelection.size = view.frame.size
        
        self.addChild(backgroundLevelSelection)
        selectlevel1 = Levels(imageNamed: stageImage)

        selectlevel1.zPosition = 1
        
        self.addChild(selectlevel1)
        selectlevel2 = Levels(imageNamed: stageImage)
        
       
        selectlevel2.zPosition = 1
        self.addChild(selectlevel2)
        selectlevel3 = Levels(imageNamed: stageImage)
       
     
      
        selectlevel3.zPosition = 1
        self.addChild(selectlevel3)
        selectlevel4 = Levels(imageNamed: stageImage)
       
       
        
        selectlevel4.zPosition = 1
        self.addChild(selectlevel4)
        selectlevel5 = Levels(imageNamed: stageImage)
        
     

        selectlevel5.zPosition = 1
        selectlevel1.isUserInteractionEnabled = true
        selectlevel2.isUserInteractionEnabled = true
        selectlevel3.isUserInteractionEnabled = true
        selectlevel5.isUserInteractionEnabled = true
        selectlevel4.isUserInteractionEnabled = true
        self.addChild(selectlevel5)
        setLevels()
        label.zPosition = 1
        label.position = CGPoint(x: self.frame.midX, y: 810)
        if stagelevel == 1 {
            label.text = "Stage 1" }
        if stagelevel == 2 {
            label.text = "Stage2" }
        
        
        label.fontSize = 30
        self.addChild(label)


    }
 

 @objc   func swipedRight(sender:UISwipeGestureRecognizer){
    if stagelevel > 1 {
        stagelevel = stagelevel - 1
        if stagelevel == 1 {
            stageImage = "moon"
            lockImage = "moon.oscure"
        }
        if stagelevel == 2 {
            stageImage = "mars"
            lockImage = "marsLocked"
        }
        setLevels()
    }
//     let changeSceneAction = SKAction.run(changeGameOverScene)
//    let waitToChangeScene = SKAction.wait(forDuration: 1)
//    let changeSceneSequence = SKAction.sequence([waitToChangeScene,changeSceneAction])
//    self.run(changeSceneSequence)
    label.text = "Stage " + String(stagelevel)
    
    }
    @objc   func swipedLeft(sender:UISwipeGestureRecognizer){
        if stagelevel < 2 {
            stagelevel = stagelevel + 1
            if stagelevel == 1 {
                stageImage = "moon"
                lockImage = "moon.oscure"
            }
            
            if stagelevel == 2 {
                stageImage = "mars"
                lockImage = "marsLocked"
            }
        
            setLevels()
            
        }
        label.text = "Stage " + String(stagelevel)
//         let changeSceneAction = SKAction.run(changeGameOverScene)
//        let waitToChangeScene = SKAction.wait(forDuration: 1)
//        let changeSceneSequence = SKAction.sequence([waitToChangeScene,changeSceneAction])
//        self.run(changeSceneSequence)
        
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches{
            print(touch.location(in: self))
        }
    }
    func setLevels(){
        
        if stageImage == "mars" {
            
            selectlevel1.name = "6"
            selectlevel2.name = "7"
            selectlevel3.name = "8"
            selectlevel4.name = "9"
            selectlevel5.name = "10"
            selectlevel1.position = CGPoint(x: 237, y: 75)
            selectlevel2.position = CGPoint(x: 114, y: 263)
            selectlevel3.position = CGPoint(x: 355, y: 414)
            selectlevel4.position = CGPoint(x: 150, y: 550)
            selectlevel5.position = CGPoint(x: 72.5, y: 795)
            backgroundLevelSelection.texture = SKTexture(imageNamed: "stage2background")
            selectlevel1.size = CGSize(width: 113, height: 87)
            
            selectlevel2.size = CGSize(width: 169, height: 130)
            selectlevel3.size = CGSize(width: 130, height: 100)
            selectlevel4.size = CGSize(width: 149.5, height: 115)

            selectlevel5.size = CGSize(width: 130, height: 100)


        }
        else {
            selectlevel1.name = "1"
            selectlevel2.name = "2"
            selectlevel3.name = "3"
            selectlevel4.name = "4"
            selectlevel5.name = "5"
            backgroundLevelSelection.texture = SKTexture(imageNamed: "stage1Background")
            selectlevel3.position = CGPoint(x: 300, y: 418)
            selectlevel1.position = CGPoint(x: 300, y: 115)
            selectlevel2.position = CGPoint(x: 102, y: 242)
            selectlevel4.position = CGPoint(x: 65, y: 564)
            selectlevel5.position = CGPoint(x: 286, y: 715)
            selectlevel1.size = CGSize(width: 85, height: 85)
            selectlevel2.size = CGSize(width: 100, height: 100)
            selectlevel3.size = CGSize(width: 120, height: 120)
            selectlevel5.size = CGSize(width: 100, height: 100)
            
            selectlevel4.size = CGSize(width: 100, height: 100)
        }
        if livelloraggiunto >= Int(selectlevel1.name!)! {
            selectlevel1.texture = SKTexture(imageNamed: stageImage)
        }
        else {
            selectlevel1.texture = SKTexture(imageNamed: lockImage)
        }
        if livelloraggiunto >= Int(selectlevel2.name!)! {
            selectlevel2.texture = SKTexture(imageNamed: stageImage)
        }
        else {
            selectlevel2.texture = SKTexture(imageNamed: lockImage)
        }
        if livelloraggiunto >= Int(selectlevel3.name!)! {
            selectlevel3.texture = SKTexture(imageNamed: stageImage)
        }
        else {
            selectlevel3.texture = SKTexture(imageNamed: lockImage)
        }
        if livelloraggiunto >= Int(selectlevel4.name!)! {
            selectlevel4.texture = SKTexture(imageNamed: stageImage)
        }
        else {
            selectlevel4.texture = SKTexture(imageNamed: lockImage)
        }
        if livelloraggiunto >= Int(selectlevel5.name!)! {
            selectlevel5.texture = SKTexture(imageNamed: stageImage)
        }
        else {
            selectlevel5.texture = SKTexture(imageNamed: lockImage)
        }
        
        
    }
    
    func changeGameOverScene(){
        
        let sceneToMoveTo = MenuScene(size: (scene?.view?.frame.size)!)
        sceneToMoveTo.scaleMode = scene!.scaleMode
        scene?.view!.presentScene(sceneToMoveTo)
    }
 
}
