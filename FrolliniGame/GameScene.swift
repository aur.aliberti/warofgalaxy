
import SpriteKit
import GameplayKit
import AVFoundation
import GameKit


var weaponSelected = 1
var gameScore : Int = 0

var cannonControllerReference : CannonController?
var manaRechargeValue = 10


class GameScene: SKScene {
    
    let background = SKSpriteNode(imageNamed: "backgroundUniverse")
    let backgroundTransiction = SKSpriteNode(imageNamed: "backgroundUniverse")
    let spaceshipTransiction = SKSpriteNode(imageNamed: "FrameNavicellaTransition")
    let cannon = SKSpriteNode(imageNamed: "CannoneLaser")
    let spaceShip = SKSpriteNode(imageNamed: "FrameNavicella")
    let dome = SKSpriteNode(imageNamed: "dome")
    let weaponsFrame = SKSpriteNode(imageNamed: "weaponframe")
    let emptyBar = SKSpriteNode(imageNamed: "LoadingFrame")
    
  
    let weaponsPanel = SKSpriteNode(imageNamed: "PanelLaser")
    let greenPanel = SKSpriteNode(imageNamed: "ChangeWeapons")
    
    let laserPanel = SKTexture(imageNamed: "PanelLaser")
    let plasmaPanel = SKTexture(imageNamed: "PanelPlasma")
    let gaussPanel = SKTexture(imageNamed: "PanelGauss")
    
    let plasmaCannoneTexture = SKTexture(imageNamed: "CannonePlasma")
    let laserCannonTexture = SKTexture(imageNamed: "CannoneLaser")
    let GaussCannoneTexture = SKTexture(imageNamed: "CannoneGauss")
    
    let blob = SKSpriteNode(imageNamed: "Blob")
    
    var frameAmmoTexture : SKTexture!
    var frameHealthTexture : SKTexture!
    
    let scorePanel = SKSpriteNode(imageNamed: "PanelScore")
    
    let manaRechargeLabel = SKLabelNode(fontNamed: "SPACEBOY")
    let scoreLabel = SKLabelNode(fontNamed: "SPACEBOY")
    let tapToStartLabel = SKLabelNode(fontNamed: "SPACEBOY")
    let noManaLabel = SKLabelNode(fontNamed: "SPACEBOY")
    
    //let percentage = SKLabelNode(fontNamed: "SPACEBOY")
    let loadingLabel = SKLabelNode(fontNamed: "SPACEBOY")
    var percentageVal = 0
    var percentageCont = SKLabelNode(fontNamed: "SPACEBOY")
    
    let quitLabel = SKLabelNode(fontNamed: "SPACEBOY")
    let resumeLabel = SKLabelNode(fontNamed: "SPACEBOY")
    
    var life = 3
    var manaBarLenght = 10
    var healthBarLenght = 3
    var timeSpawn: Float = 5
    var preTimeSpawn: Float = 5
    var repeatsSpawn: Int = 1
    var durationEnemy: Float = 60
    var velocityEnemy: CGFloat = 0
    /**************SOUNDS EFFECT********************/
    var explosionSound = SKAction.playSoundFileNamed("explosion", waitForCompletion: false)
    var enemyHitSound = SKAction.playSoundFileNamed("enemyHitSound-3", waitForCompletion: false)
    var normalEnemySound = SKAction.playSoundFileNamed("dino1", waitForCompletion: false)
    var mediumEnemySound = SKAction.playSoundFileNamed("dino3", waitForCompletion: false)
    var hardEnemySound = SKAction.playSoundFileNamed("dino2", waitForCompletion: false)
    var normalBulletSound = SKAction.playSoundFileNamed("laser1-1", waitForCompletion: false)
    var mediumBulletSound = SKAction.playSoundFileNamed("plasmaSound.wav", waitForCompletion: false)
    var hardBulletSound = SKAction.playSoundFileNamed("laser3", waitForCompletion: false)
    var changeWeaponEffect = SKAction.playSoundFileNamed("changeWeaponsSound3", waitForCompletion: false)
    
    
    
    
    var backgroundSound = SKAudioNode(fileNamed: "brundubeat.wav")
    var gameOverSound = SKAudioNode(fileNamed: "gameoverSound.wav")
 
    let recoverLifeSound = SKAction.playSoundFileNamed("lifeRecover-4", waitForCompletion: false)
    
    var manaBar = [SKSpriteNode]()
    var healthBar = [SKSpriteNode]()
    
    enum GameState : Int{
        
        case preGame = -1
        case inGame = 0
        case afterGame = 1
    }
    
    
 
    
    
    var currentGameState = GameState.preGame
    
    var enemies = [Enemy]()
    
    var lineGuide = SKSpriteNode()
    var directTouch = false
    var fingerLocation = CGPoint()
    
    var normalGunActive = true
    var mediumGunActive = false
    var hardGunActive = false
    
    var enemyIndex = -1
    var healthIndex = -1
    var addOnManaIndex = -1
    
    

    
    var gameArea: CGRect
    
    override init(size: CGSize) {
        
        gameArea = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        super.init(size: size)
        
        print("Height: \(size.height), Width: \(size.width)")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var spawningPositions = [CGPoint]()
    
    override func didMove(to view: SKView) {
        print("CIAO")
        currentGameState = GameState.preGame
        
        
        self.physicsWorld.contactDelegate = self
        
        weaponsPanel.name = "weaponsPanel"
        
        setupTapToBeginLabel()
        setupBackground()
        setupLoadingTransiction()
        setupSpaceShip()
        setupScorePanel()
        setupBorders(view: view)
        setupSpawningPositions()
        setupManaRechargeLabel()
     
        setupPauseButton()
        setupResumeQuitLabel()
       
        
    
        
        manaBar = setupSpaceShipFrame(lenght: manaBarLenght, parent: self)
        healthBar = setupHealthBar(lenght: healthBarLenght , parent: self)
        
        
        let cannonController = CannonController(textureName: "Barretta", cannon: setupCannon(), manaBarLenght: manaBarLenght, manaBar: manaBar)
        
        cannonController.zPosition = 7
        cannonController.setScale(1.2)
       
        cannonController.name = "controller"
        cannonController.position.x = self.frame.width / 2
        cannonController.position.y = 65
        self.addChild(cannonController)
        cannonController.startManaRecovery()
        
        cannonControllerReference = cannonController
        
        
        
        setupControlPanel(cannonController: cannonController)
       }
    
    func setupPauseButton(){
        
        let pauseButton = SKSpriteNode(imageNamed: "Pause")
        
        pauseButton.zPosition = 10
        pauseButton.position.x = 380
        pauseButton.position.y = 150
        pauseButton.setScale(0.7)
        pauseButton.name = "pauseButton"
        self.addChild(pauseButton)
        
        
        
    }
    
  
    func setupManaRechargeLabel(){
        
        
        manaRechargeLabel.text = "\(manaRechargeValue)"
        manaRechargeLabel.name = "manaRechargeLabel"
        manaRechargeLabel.fontSize = 25
        manaRechargeLabel.fontColor = .green
        manaRechargeLabel.zPosition = 6
        manaRechargeLabel.position = CGPoint(x: 350, y: 80)
        manaRechargeLabel.alpha = 1
        self.addChild(manaRechargeLabel)
        
        
    }
    
    
    func setupTapToBeginLabel(){
        
        tapToStartLabel.text = "Tap to Begin"
        tapToStartLabel.name = "tapToStart"
        
        tapToStartLabel.fontSize = 25
        tapToStartLabel.fontColor = .green
        tapToStartLabel.zPosition = 5
        tapToStartLabel.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        tapToStartLabel.alpha = 0
        self.addChild(tapToStartLabel)
        
        let fadeInAction = SKAction.fadeIn(withDuration: 0.5)
        tapToStartLabel.run(fadeInAction)
        
        let scaleInAction = SKAction.scale(to: 1.2, duration: 1)
        let scaleOutAction = SKAction.scale(to: 1.1, duration: 1)
        let scaleSequence = SKAction.sequence([scaleInAction,scaleOutAction])
        
        tapToStartLabel.run(SKAction.repeatForever(scaleSequence))
        
    }

    
    // MARK: SETUP FUNCTIONS
    
   
    
    func setupBackground() {
        
        background.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        background.size.height = self.size.height
        background.size.width = self.size.width
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.zPosition = -5
        
        self.addChild(background)
    }
    
    
    
    
    
     let progressBar = SKSpriteNode(imageNamed: "Loading")
    
    func setupLoadingTransiction(){
        
        
       
        backgroundTransiction.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        backgroundTransiction.size.height = self.size.height
        backgroundTransiction.size.width = self.size.width
        backgroundTransiction.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        backgroundTransiction.zPosition = 12
        self.addChild(backgroundTransiction)
        
        
        
        spaceshipTransiction.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        spaceshipTransiction.size.height = self.size.height
        spaceshipTransiction.size.width = self.size.width
        spaceshipTransiction.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        spaceshipTransiction.zPosition = 18
        self.addChild(spaceshipTransiction)
        
        
        
        
        
        emptyBar.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 6)
        emptyBar.setScale(0.9)
        emptyBar.zPosition = 14
        self.addChild(emptyBar)
        
        //progressBar.color = .green
        progressBar.size.width = emptyBar.size.width - 10
        progressBar.xScale = 0
        progressBar.yScale = 1
        progressBar.zPosition = 13
        progressBar.anchorPoint = CGPoint(x: 0.0, y: 0.5)
        progressBar.position.x = (emptyBar.position.x - emptyBar.frame.width / 2) + 5
        progressBar.position.y = emptyBar.position.y
        self.addChild(progressBar)
        
        startProgressBar()
        
        percentageCont.position.x = self.frame.width / 2
        percentageCont.position.y = emptyBar.position.y + 80
        percentageCont.zPosition = 15
        percentageCont.fontSize = 18
        percentageCont.fontColor = .green
        self.addChild(percentageCont)
        
        startLoadingPercentage()
        
        print(percentageVal)
        
        

        }
    
    
   func startLoadingPercentage(){
    
    let tutorialType = Int.random(in: 1...3)
    var primaryNode = SKSpriteNode()
    var secondaryNode = SKSpriteNode()
    
    primaryNode.zPosition = 15
    
    
    let gameLabel = SKLabelNode(fontNamed: "SPACEBOY")
    let gameLabel2 = SKLabelNode(fontNamed: "SPACEBOY")
    
    gameLabel.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 1.3)
    gameLabel.zPosition = 15
    gameLabel.fontSize = 20
    gameLabel.fontColor = .green
    
    gameLabel2.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2.5)
    gameLabel2.fontColor = .green
    gameLabel2.zPosition = 15
    gameLabel2.fontSize = 20
    
    
    switch tutorialType{
        
    case 1:
        

        primaryNode = SKSpriteNode(texture: SKTexture(imageNamed: "tutorialMana"))
        primaryNode.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 1.7)
        primaryNode.zPosition = 18
       
        
        
        gameLabel.text = "Check your Ammo"
        gameLabel2.text = "before shooting"
        
        
        
        
        
    case 2:
        
        
        primaryNode = SKSpriteNode(texture: SKTexture(imageNamed: "Mana+3"))
        primaryNode.position = CGPoint(x: self.frame.width / 3, y: self.frame.height / 1.5)
        primaryNode.zPosition = 18
        primaryNode.setScale(1.3)
        
        
        secondaryNode = SKSpriteNode(texture: SKTexture(imageNamed: "Health+1"))
        secondaryNode.position = CGPoint(x: self.frame.width / 1.5, y: self.frame.height / 1.6)
        secondaryNode.zPosition = 18
        secondaryNode.setScale(1.3)
        
        
        
        gameLabel.text = "Try to take Ammo"
        gameLabel2.text = "and Health refill"
        
        let scaleIn = SKAction.scale(to: 1.5, duration: 1)
        let scaleOut = SKAction.scale(to: 1.2, duration: 1)
        
        let sequence = SKAction.sequence([scaleIn,scaleOut])
        
        primaryNode.run(SKAction.repeatForever(sequence))
        secondaryNode.run(SKAction.repeatForever(sequence))
        
        
        
        
        
    case 3:
        
        
        primaryNode = SKSpriteNode(imageNamed: "ChangeWeapons")
        primaryNode.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 1.7)
        primaryNode.zPosition = 18
        primaryNode.setScale(1.5)
        
        secondaryNode = SKSpriteNode(texture: SKTexture(imageNamed: "PanelLaser"))
        secondaryNode.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 1.7)
        secondaryNode.zPosition = 18
        secondaryNode.setScale(1.5)
        
        
        
        
        gameLabel.text = "Change weapons "
        
        
        gameLabel2.text = "tapping the panel"
        
        
        
        
    default:
        
        print("Errore transiction")
        
        
    }
    
    self.addChild(primaryNode)
    self.addChild(gameLabel)
    self.addChild(secondaryNode)
    self.addChild(gameLabel2)
        
        let fadeOut = SKAction.fadeOut(withDuration: 0.5)
        
        let increasePercentage = SKAction.run {
          
            self.percentageVal += 1
            self.percentageCont.text = "Loading...\(self.percentageVal)%"
            
            if self.percentageVal == 100{
                
                self.emptyBar.run(fadeOut)
                self.progressBar.run(fadeOut)
                self.percentageCont.run(fadeOut)
                
                
                self.backgroundTransiction.run(SKAction.fadeOut(withDuration: 3))
                primaryNode.run(fadeOut)
                gameLabel2.run(fadeOut)
                gameLabel.run(fadeOut)
                secondaryNode.run(fadeOut)
                self.spaceshipTransiction.run(fadeOut)
                }
            
        }
        
        let scalePercSequence = SKAction.sequence([increasePercentage,SKAction.wait(forDuration: 0.05)])
        let repeatSequence = SKAction.repeat(scalePercSequence, count: 100)
        
        progressBar.run(repeatSequence)
        
        }
    
    
    
    func startProgressBar(){
        
        let scaleX = SKAction.scaleX(to: 1, duration: 5)
        
        progressBar.run(scaleX)
        
        }
    
    func setupSpaceShip() {
        
        spaceShip.size.width = self.frame.width
        spaceShip.size.height = self.size.height
        spaceShip.zPosition = 4
        spaceShip.position = CGPoint(x: self.frame.width / 2 , y: spaceShip.size.height / 2)
        self.addChild(spaceShip)
    }
    
    func setupResumeQuitLabel(){
        
        quitLabel.text = "Quit"
        quitLabel.fontSize = 30
        quitLabel.fontColor = SKColor.green
        quitLabel.zPosition = 30
        quitLabel.alpha = 0
        quitLabel.position = CGPoint(x: self.size.width/2, y: self.size.height / 3)
        self.addChild(quitLabel)
        
        
        resumeLabel.text = "Resume"
        resumeLabel.fontSize = 30
        resumeLabel.alpha = 0
        resumeLabel.fontColor = SKColor.green
        resumeLabel.zPosition = 30
        resumeLabel.position = CGPoint(x: self.size.width/2, y: self.size.height / 1.8)
        self.addChild(resumeLabel)
    }


    
    func setupScorePanel(){
        
        
        scorePanel.position.x = self.frame.width / 2 - 5
        scorePanel.position.y = 910
        scorePanel.zPosition = 3
        self.addChild(scorePanel)
        
        scoreLabel.text = "\(gameScore)"
        scoreLabel.fontSize = 20
        scoreLabel.alpha = 0
        scoreLabel.fontColor = SKColor.green
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        scoreLabel.position = CGPoint(x: self.frame.width / 2, y: 840)
        scoreLabel.zPosition = 10
        self.addChild(scoreLabel)
        
        }
    
    
    func setupCannon() -> SKSpriteNode{
        
        cannon.zPosition = 0
        cannon.name = "cannon"
        cannon.position = CGPoint(x: self.size.width/2, y: 0)
        cannon.anchorPoint = CGPoint(x: 0.5, y: 0)
        cannon.physicsBody = SKPhysicsBody(rectangleOf: cannon.size)
        cannon.physicsBody!.affectedByGravity = false
        cannon.physicsBody!.categoryBitMask = PhysicsCategories.SpaceShip
        cannon.physicsBody!.collisionBitMask = PhysicsCategories.None
        cannon.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyNormal
        cannon.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyMedium
        cannon.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyHard
        
        self.addChild(cannon)
        
        return cannon
        
        }
    
    
    
   
    
   
    
  
    
    //    func setupManaBar(barLenght: Int, barPosition: CGPoint, parent: SKNode) -> [SKSpriteNode]{
    //
    //
    //        var barContainer = [SKSpriteNode]()
    //
    //
    //        for i in 0...barLenght {
    //
    //            let greenBar = SKSpriteNode(imageNamed: "Ammo1")
    //
    //
    //
    //            barContainer.append(greenBar)
    //            barContainer[i].zPosition = 2
    //
    //            barContainer[i].size = CGSize(width: 42, height: 18)
    //
    //            if i > 0 {
    //                barContainer[i].position = CGPoint(x: barContainer[i - 1].position.x + barContainer[i].size.width/2, y: barPosition.y - 50)
    //            }
    //            else {
    //                barContainer[i].position = barPosition
    //            }
    //
    //            parent.addChild(barContainer[i])
    //        }
    //        return barContainer
    //    }
    
    
    func setupControlPanel(cannonController: CannonController){
        
        
       
        greenPanel.name = "greenPanel"
        greenPanel.zPosition = 6
        greenPanel.position.x = 60
        greenPanel.position.y = 85
       
        self.addChild(greenPanel)
        
        weaponsPanel.zPosition = 7
        weaponsPanel.position.x = 60
        weaponsPanel.position.y = 85
        weaponsPanel.alpha = 0
        self.addChild(weaponsPanel)
        
        let blob = SKSpriteNode(imageNamed: "Blob")
        blob.name = "Blob"
        blob.zPosition = 5
        blob.position.x = 350
        blob.position.y = 85
        
        self.addChild(blob)
        
        }
    
    func setupHealthBar(lenght: Int, parent: SKNode) -> [SKSpriteNode]{
        
        var healthBarContainer = [SKSpriteNode]()
        var healthBarContainerPositions = [CGPoint]()
        
        healthBarContainerPositions.append(CGPoint(x: 388, y: 250))
        healthBarContainerPositions.append(CGPoint(x: 344, y: 202))
        healthBarContainerPositions.append(CGPoint(x: 283, y: 165))
        
        
        for i in 0...lenght - 1 {
            
            let name = "Health" + String(i+1)
            let node = SKSpriteNode(imageNamed: name)
            node.position = healthBarContainerPositions[i]
            node.zPosition = 2
            healthBarContainer.append(node)
            parent.addChild(healthBarContainer[i])
            
        }
        
        return healthBarContainer
        }
    
    
    func setupSpaceShipFrame(lenght: Int, parent: SKNode) -> [SKSpriteNode]{
        
       
        frameAmmoTexture = SKTexture(imageNamed: "frameAmmo")
       // frameAmmoTexture.filteringMode = .nearest
        
        let frameAmmo = SKSpriteNode(texture: frameAmmoTexture)
        frameAmmo.physicsBody = SKPhysicsBody(texture: frameAmmoTexture, size: frameAmmoTexture.size())
        frameAmmo.physicsBody?.affectedByGravity = false
        frameAmmo.physicsBody!.categoryBitMask = PhysicsCategories.SpaceShip
        frameAmmo.physicsBody!.collisionBitMask = PhysicsCategories.None
        frameAmmo.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyNormal
        frameAmmo.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyMedium
        frameAmmo.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyHard
        frameAmmo.position = CGPoint(x: 72, y: 208)
        frameAmmo.zRotation = -0.66 //più aumenta in negativo più il nodo gira in senso antiorario
        frameAmmo.zPosition = 3
        self.addChild(frameAmmo)
        
        frameHealthTexture = SKTexture(imageNamed: "frameHealth")
        //frameHealthTexture.filteringMode = .nearest
        
        let frameHealth = SKSpriteNode(texture: frameHealthTexture)
        frameHealth.physicsBody = SKPhysicsBody(texture: frameHealthTexture, size: frameHealthTexture.size())
        frameHealth.physicsBody?.affectedByGravity = false
        frameHealth.physicsBody!.categoryBitMask = PhysicsCategories.SpaceShip
        frameHealth.physicsBody!.collisionBitMask = PhysicsCategories.None
        frameHealth.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyNormal
        frameHealth.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyMedium
        frameHealth.physicsBody!.contactTestBitMask = PhysicsCategories.EnemyHard
        frameHealth.position = CGPoint(x: 341, y: 208)
        frameHealth.zRotation = 0.66 // più aumenti più ruota in senso antiorario
        frameHealth.zPosition = 3
        self.addChild(frameHealth)
        
        
        var ammoBarContainer = [SKSpriteNode]()
        var ammoBarContainerPositions = [CGPoint]()
        
        ammoBarContainerPositions.append(CGPoint(x: 13, y: 270))
        ammoBarContainerPositions.append(CGPoint(x: 21, y: 251))
        ammoBarContainerPositions.append(CGPoint(x: 31, y: 235))
        ammoBarContainerPositions.append(CGPoint(x: 44, y: 221))
        ammoBarContainerPositions.append(CGPoint(x: 59, y: 208))
        ammoBarContainerPositions.append(CGPoint(x: 76, y: 193))
        ammoBarContainerPositions.append(CGPoint(x: 94, y: 183))
        ammoBarContainerPositions.append(CGPoint(x: 115, y: 173))
        ammoBarContainerPositions.append(CGPoint(x: 135, y: 163))
        ammoBarContainerPositions.append(CGPoint(x: 153, y: 157))

        
        for i in 0...lenght - 1 {
            
            let name = "Ammo" + String(i+1)
            let node = SKSpriteNode(imageNamed: name)
            node.position = ammoBarContainerPositions[i]
            node.zPosition = 2
            ammoBarContainer.insert(node, at: i)
            parent.addChild(ammoBarContainer[i])
        }
        return ammoBarContainer
        
    }
    
    
    
    func setupBorders(view : SKView) {
        
        let rightBorder = SKSpriteNode()
        rightBorder.name = "rightBorder"
        
        rightBorder.position = CGPoint(x: view.frame.maxX  , y: view.frame.midY)
        rightBorder.size = CGSize(width: 1, height: view.frame.height)
        
        rightBorder.physicsBody = SKPhysicsBody(rectangleOf: rightBorder.size)
        rightBorder.zPosition = 1
        rightBorder.physicsBody!.categoryBitMask = PhysicsCategories.Border
        rightBorder.physicsBody!.collisionBitMask = PhysicsCategories.NormalBullet | PhysicsCategories.MediumBullet | PhysicsCategories.HardBullet
        rightBorder.physicsBody!.contactTestBitMask = PhysicsCategories.NormalBullet | PhysicsCategories.MediumBullet | PhysicsCategories.HardBullet
        rightBorder.physicsBody?.restitution = 1
        rightBorder.physicsBody?.friction = 0.0
        rightBorder.physicsBody?.isDynamic = false
        rightBorder.physicsBody?.mass = 0
        rightBorder.physicsBody?.linearDamping = 0
        rightBorder.physicsBody?.angularDamping = 0
        rightBorder.physicsBody?.affectedByGravity = false
        
        
        self.addChild(rightBorder)
        
        
        let leftBorder = SKSpriteNode()
        leftBorder.name = "leftBorder"
        
        leftBorder.position = CGPoint(x: view.frame.minX  , y: view.frame.midY)
        leftBorder.size = CGSize(width: 1, height: view.frame.height)
        
        leftBorder.physicsBody = SKPhysicsBody(rectangleOf: leftBorder.size)
        leftBorder.zPosition = 1
        leftBorder.physicsBody!.categoryBitMask = PhysicsCategories.Border
        leftBorder.physicsBody!.collisionBitMask = PhysicsCategories.NormalBullet | PhysicsCategories.MediumBullet | PhysicsCategories.HardBullet
        leftBorder.physicsBody!.contactTestBitMask = PhysicsCategories.NormalBullet | PhysicsCategories.MediumBullet | PhysicsCategories.HardBullet
        leftBorder.physicsBody?.restitution = 1
        leftBorder.physicsBody?.friction = 0.0
        leftBorder.physicsBody?.isDynamic = false
        leftBorder.physicsBody?.mass = 0
        leftBorder.physicsBody?.linearDamping = 0
        leftBorder.physicsBody?.angularDamping = 0
        leftBorder.physicsBody?.affectedByGravity = false
        self.addChild(leftBorder)
        
        let topBorder = SKSpriteNode()
        topBorder.name = "topBorder"
        
        topBorder.position = CGPoint(x: view.frame.midX  , y: view.frame.maxY)
        topBorder.size = CGSize(width: view.frame.width, height: 1)
        
        topBorder.physicsBody = SKPhysicsBody(rectangleOf: topBorder.size)
        topBorder.zPosition = 1
        topBorder.physicsBody!.categoryBitMask = PhysicsCategories.Border
        topBorder.physicsBody!.collisionBitMask = PhysicsCategories.NormalBullet | PhysicsCategories.MediumBullet | PhysicsCategories.HardBullet
        topBorder.physicsBody!.contactTestBitMask = PhysicsCategories.NormalBullet | PhysicsCategories.MediumBullet | PhysicsCategories.HardBullet
        topBorder.physicsBody?.restitution = 1
        topBorder.physicsBody?.friction = 0.0
        topBorder.physicsBody?.isDynamic = false
        topBorder.physicsBody?.mass = 0
        topBorder.physicsBody?.linearDamping = 0
        topBorder.physicsBody?.angularDamping = 0
        topBorder.physicsBody?.affectedByGravity = false
        self.addChild(topBorder)
        

    }
   
    
    func setupSpawningPositions() {
        
        let screenWidthMax = Int(gameArea.maxX)
        let increment = 60
        var positionIndex = 0
        
        while ( positionIndex < screenWidthMax - increment ) {
            
            spawningPositions.append(CGPoint(x: positionIndex + increment, y: Int(self.size.height)))
            
            print("Posizione di spawning: \(positionIndex + increment)")
            
            positionIndex += increment
        }
    }
    
    
    func startGame(){
        
        currentGameState = GameState.inGame
        cannonControllerReference?.gameState = 0
        
        self.addChild(backgroundSound)
        startEnemySpawn()
        
        let volumeUP = SKAction.changeVolume(to: 10, duration: 0.0)
        backgroundSound.run(volumeUP)
        
        
        let fadeInPanel = SKAction.fadeIn(withDuration: 1)
        let fadeOutAction = SKAction.fadeOut(withDuration: 0.8)
        let deleteAction = SKAction.removeFromParent()
        let deleteSequence = SKAction.sequence([fadeOutAction, deleteAction])
        
        tapToStartLabel.run(deleteSequence)
        
        let moveScorePanelInAction = SKAction.moveTo(y: 870, duration: 1)
        let moveCannonToScreenAction = SKAction.moveTo(y: 130, duration: 1.2)
        let scoreLabelAlphaAction = SKAction.fadeIn(withDuration: 10)
        
        
        weaponsPanel.run(fadeInPanel)
        scorePanel.run(moveScorePanelInAction)
        scoreLabel.run(scoreLabelAlphaAction)
        cannon.run(moveCannonToScreenAction)
        
    }
    
    
    
    
    
    //****************************ENEMY SPAWN
    func startEnemySpawn() {
        if currentGameState == GameState.inGame {
        let spawnEnemy = SKAction.run(searchSpawningPosition)
        let waitToSpawn = SKAction.wait(forDuration: TimeInterval(timeSpawn))
        let spawnSequence = SKAction.sequence([spawnEnemy,waitToSpawn])
        let spawn = SKAction.repeat(spawnSequence, count: repeatsSpawn)
        self.run(spawn, completion: decreaseTimer )
        
        }
        else {
            let waitToSpawn = SKAction.wait(forDuration: TimeInterval(4))
            let spawn = SKAction.repeat(waitToSpawn, count: 1)
            self.run(spawn, completion: decreaseTimer)
            
        }
    }
    func decreaseTimer() {
        if (gameScore >= 25 && gameScore < 50 && timeSpawn == 5.0){
            timeSpawn = 4.7
            preTimeSpawn = timeSpawn
            durationEnemy = 55
            startEnemySpawn()
        }
        else if (gameScore >= 50 && gameScore < 75 && timeSpawn == 4.7){
                timeSpawn = 4.4
                preTimeSpawn = timeSpawn
                durationEnemy = 45
                startEnemySpawn()
            
        }
        else if (gameScore >= 75 && gameScore <= 100 && timeSpawn == 4.4){
            timeSpawn = 4.1
            preTimeSpawn = timeSpawn
            durationEnemy = 40
            startEnemySpawn()
            
        }else if (gameScore >= 100 && gameScore < 150 && timeSpawn == 4.1){
            timeSpawn = 3.8
            preTimeSpawn = timeSpawn
            durationEnemy = 35
            startEnemySpawn()
            
        }else if (gameScore >= 150 && timeSpawn == 3.8){
            preTimeSpawn = timeSpawn
            timeSpawn = 3.5
            durationEnemy = 30
            startEnemySpawn() }
        
        else {
            startEnemySpawn()
        }
        
    }
    override func update(_ currentTime: TimeInterval) {
   
    }
    let scaleIn = SKAction.scale(to: 0.25, duration: 3)
    
    func enemySpawn(enemyType : EnemyType, position : CGPoint) -> Enemy{
        
        
        let endPoint = CGPoint(x: position.x , y: -self.size.height * 0.2)
        
        //enemyIndex += 1
        
        var health = 1
        var textureName = ""
        var physicsCategory = PhysicsCategories.EnemyNormal
        
        switch enemyType {
        case .normal:
            health = 1
            textureName = "blueEnemy"
            physicsCategory = PhysicsCategories.EnemyNormal
        case .medium:
            health = 3
            textureName = "greenEnemy"
            physicsCategory = PhysicsCategories.EnemyMedium
        case .hard:
            health = 5
            textureName = "redEnemy"
            physicsCategory = PhysicsCategories.EnemyHard
        case .health:
            health = -1
            textureName = "Health+1"
            physicsCategory = PhysicsCategories.PowerUp
        case .addOnMana:
            health = 0
            textureName = "Mana+3"
            physicsCategory = PhysicsCategories.PowerUp
            
        }
        
        
        let enemy = Enemy(name: textureName, health: health, enemyType: enemyType)
        
        
        enemy.setScale(0.01)

       
        enemy.position = position
        enemy.zPosition = 1
        
        enemy.physicsBody = SKPhysicsBody(rectangleOf: enemy.size)
        enemy.physicsBody?.categoryBitMask = physicsCategory
        enemy.physicsBody!.collisionBitMask = PhysicsCategories.None
        enemy.physicsBody!.contactTestBitMask = PhysicsCategories.SpaceShip
        enemy.physicsBody!.contactTestBitMask =  PhysicsCategories.NormalBullet
        enemy.physicsBody!.contactTestBitMask =  PhysicsCategories.MediumBullet
        enemy.physicsBody!.contactTestBitMask =  PhysicsCategories.HardBullet
        enemy.physicsBody!.affectedByGravity = false
        enemy.physicsBody?.allowsRotation = false
        enemy.physicsBody?.friction = 0
        enemy.physicsBody?.isDynamic = false
        
        self.addChild(enemy)
        enemy.playFireAnimation()
        
        let moveEnemy = SKAction.move(to: endPoint, duration: TimeInterval(durationEnemy))
        let moveAndResize = SKAction.group([scaleIn,moveEnemy])
        let deleteEnemy = SKAction.removeFromParent()
        let enemySequence = SKAction.sequence([moveAndResize,deleteEnemy])
        
        if currentGameState == GameState.inGame{
            enemy.run(enemySequence)
            
        }
        return enemy
    }
    //**********************************************
    
  
    //***********************TOUCHES BEGAN
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        for touch in touches {
            
            let location = touch.location(in: self)
            let touchedNode = self.atPoint(location)
            
            if currentGameState == GameState.inGame{
                
                if touchedNode.name == "weaponsPanel"{
                    
                    switch weaponSelected{
                    case 1:
                        changeWeaponsPanel(panelToChangeTo: 2)
                        changeCannonTo(cannonToChangeTo: 2)
                        playSound(sound: changeWeaponEffect)
                        weaponSelected = 2
                    case 2:
                        changeWeaponsPanel(panelToChangeTo: 3)
                        changeCannonTo(cannonToChangeTo: 3)
                        playSound(sound: changeWeaponEffect)
                        weaponSelected = 3
                    case 3:
                        changeWeaponsPanel(panelToChangeTo: 1)
                        changeCannonTo(cannonToChangeTo: 1)
                        playSound(sound: changeWeaponEffect)
                        weaponSelected = 1
                    default:
                        changeWeaponsPanel(panelToChangeTo: 1)
                        changeCannonTo(cannonToChangeTo: 1)
                        playSound(sound: changeWeaponEffect)
                        weaponSelected = 1
                    }
                }
                    
                else if (touchedNode.name == "manaRechargeLabel" || touchedNode.name == "Blob") && manaRechargeValue > 0{
                    
                    cannonControllerReference?.addMana()
                    manaRechargeLabel.text = "\(manaRechargeValue)"
                    playSound(sound: chargingMana)
                }
                    
                else if touchedNode.name == "pauseButton"{
                    
                    let fadeIn = SKAction.fadeIn(withDuration: 0.3)
                    
                    quitLabel.run(fadeIn)
                    resumeLabel.run(fadeIn)
                    timeSpawn = 10
                    
                    for nodeChild in scene!.children{
                        if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyHard || nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyMedium || nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyNormal || nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.PowerUp {
                            velocityEnemy = (nodeChild.speed)
                            nodeChild.speed = 0
                            
                        }
                    }
                    currentGameState = GameState.afterGame
                }
            }
            else if currentGameState == GameState.afterGame {
                if resumeLabel.contains(location){
                    
                    resumeLabel.run(SKAction.fadeOut(withDuration: 0.1))
                    quitLabel.run(SKAction.fadeOut(withDuration: 0.1))
                    
                    
                    for nodeChild in scene!.children{
                        if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyHard || nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyMedium || nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyNormal || nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.PowerUp {
                            nodeChild.speed = velocityEnemy
                        }
                    }
                    currentGameState = GameState.inGame
                    timeSpawn = preTimeSpawn
                   
                }
                else if quitLabel.contains(location){
                    
                    runGameOver()
                    
                }
            }
                
            else if currentGameState == GameState.preGame && percentageVal == 100 {
                
                startGame()
                
                
            }
        }
    }
    
 
    
    
    
    /*override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        for touch in touches {
            let location = touches.first?.location(in: self)
            let touchedNode = self.atPoint(location!)
            
            if touchedNode.name != "gun1" && touchedNode.name != "gun2" && touchedNode.name != "gun3" {
                fingerLocation = touch.location(in: self)
                setLineGuide()
                
            }
        }
    }*/
    
    
    
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//        for touch in touches {
//
//            let location = touch.location(in: self)
//            let touchedNode = self.atPoint(location)
//
//            if normalGunActive == true {
//
//                if directTouch == true && touchedNode.name != "gun1" && touchedNode.name != "gun2" && touchedNode.name != "gun3"{
//
//                    if currentGameState == gameState.inGame{
//                        fingerLocation = touch.location(in: self)
//                        lineGuide.removeFromParent()
//
//                        if manaBarLenght > -1 {
//                            fireNormalBullet()
//                        }
//                        directTouch = false
//
//                    }
//                }
//
//            }else if mediumGunActive == true  && touchedNode.name != "gun1" && touchedNode.name != "gun2" && touchedNode.name != "gun3"  {
//
//                if currentGameState == gameState.inGame{
//                    if directTouch == true {
//
//                        lineGuide.removeFromParent()
//                        if manaBarLenght > 0 {
//                            fireMediumBullet()
//                        }
//                        directTouch = false
//
//                    }
//                }
//            }
//        }
//
//    }
    
    func changeCannonTo(cannonToChangeTo: Int){
        
        
        let moveCannonDown = SKAction.moveTo(y: 20, duration: 0.5)
        let changeTextureCannon : SKAction
        
        switch cannonToChangeTo{
        case 1:
            changeTextureCannon = SKAction.setTexture(laserCannonTexture)
        case 2:
            changeTextureCannon = SKAction.setTexture(plasmaCannoneTexture)
        case 3:
            changeTextureCannon = SKAction.setTexture(GaussCannoneTexture)
        default:
            changeTextureCannon = SKAction.setTexture(laserCannonTexture)
        }
        
        
        let moveCannonUp = SKAction.moveTo(y: 130, duration: 0.5)
        
        let changeCannonSequence = SKAction.sequence([moveCannonDown,changeTextureCannon,moveCannonUp])
        cannon.run(changeCannonSequence)
        
        }
    
    func changeAmmoLabel(){}
    
    
    func changeWeaponsPanel(panelToChangeTo: Int){
        
        let changeTexturePanel : SKAction
        
        switch panelToChangeTo{
            
        case 1:
            changeTexturePanel = SKAction.setTexture(laserPanel)
        case 2:
            changeTexturePanel = SKAction.setTexture(plasmaPanel)
        case 3:
            changeTexturePanel = SKAction.setTexture(gaussPanel)
        default:
            changeTexturePanel = SKAction.setTexture(laserPanel)
        }
        
        let fadeOut = SKAction.fadeOut(withDuration: 1)
        let fadeIn = SKAction.fadeIn(withDuration: 1)
        
        let changePanelSequence = SKAction.sequence([fadeOut,changeTexturePanel,fadeIn])
        weaponsPanel.run(changePanelSequence)
        
        }
    
    
    
    
        
        
    
//
//    func changeToPlasma(){
//
////        let plasmaTexture = SKTexture(imageNamed: "CannonePlasma")
////        let fadeOut = SKAction.fadeOut(withDuration: 2)
////        let fadeIn = SKAction.fadeAlpha(to: 1, duration: 2)
////
////        laserPanel.run(fadeOut)
////        laserPanel.removeFromParent()
////
////        plasmaPanel.zPosition = 7
////        plasmaPanel.position = CGPoint(x: 60,y: 85)
////        plasmaPanel.alpha = 0
////        self.addChild(plasmaPanel)
////
////        plasmaPanel.run(fadeIn)
////
////
////
//    }
//
//
//    func changeTexture(){
//
//        print("Ciao")
//
//        }
//
//
//    func changeToGauss(){
//
//
//
//        let fadeOut = SKAction.fadeOut(withDuration: 2)
//        let fadeIn = SKAction.fadeAlpha(to: 1, duration: 2)
//
//        plasmaPanel.run(fadeOut)
//        plasmaPanel.removeFromParent()
//        gaussPanel.zPosition = 7
//        gaussPanel.position = CGPoint(x: 60,y: 85)
//
//
//        self.addChild(gaussPanel)
//        gaussPanel.run(fadeIn)
//    }
//
//    func changeToLaser(){
//
//
//
//        let fadeOut = SKAction.fadeOut(withDuration: 2)
//        let fadeIn = SKAction.fadeAlpha(to: 1, duration: 2)
//
//
//        gaussPanel.run(fadeOut)
//        gaussPanel.removeFromParent()
//        laserPanel.zPosition = 7
//        laserPanel.position = CGPoint(x: 60,y: 85)
//
//
//        self.addChild(laserPanel)
//        laserPanel.run(fadeIn)
//
//    }
    
//    func setLineGuide() {
//
//        lineGuide.removeFromParent()
//        lineGuide = SKSpriteNode(imageNamed: "linea")
//        lineGuide.position = CGPoint(x: cannon.position.x , y: cannon.position.y)
//        lineGuide.anchorPoint = CGPoint(x: 0.5, y: 0)
//        lineGuide.zPosition = 2
//        lineGuide.size = CGSize(width: 0.5, height: 1000)
//        lineGuide.physicsBody = SKPhysicsBody(circleOfRadius: lineGuide.size.width/2)
//        lineGuide.physicsBody?.affectedByGravity = false
//        lineGuide.physicsBody?.restitution = 0
//        lineGuide.physicsBody?.allowsRotation = false
//        lineGuide.physicsBody?.categoryBitMask = PhysicsCategories.NormalBullet
//        lineGuide.physicsBody?.collisionBitMask = PhysicsCategories.None
//        lineGuide.removeFromParent()
//        cannon.zRotation = findAngle()
//
//        lineGuide.zRotation = findAngle()
//        self.addChild(lineGuide)
//    }
    
    
//
//    func fireNormalBullet() {
//
//        let normalBullet = NormalBullet(imageNamed: "LaserBullet")
//        playSound(sound: normalBulletSound)
//        normalBullet.position = CGPoint(x: cannon.position.x , y: cannon.position.y + 130)
//        normalBullet.size = CGSize(width: 30, height: 45)
//        normalBullet.physicsBody = SKPhysicsBody(circleOfRadius: normalBullet.size.width/2)
//        normalBullet.physicsBody?.affectedByGravity = false
//        normalBullet.zPosition = 1
//        normalBullet.zRotation = findAngle()
//        normalBullet.physicsBody?.restitution = 0
//        normalBullet.physicsBody?.friction = 0
//        normalBullet.physicsBody?.angularDamping = 0
//        normalBullet.physicsBody?.linearDamping = 0
//        normalBullet.physicsBody?.allowsRotation = false
//        normalBullet.physicsBody?.usesPreciseCollisionDetection = true
//        normalBullet.physicsBody?.categoryBitMask = PhysicsCategories.NormalBullet
//        normalBullet.physicsBody?.collisionBitMask = PhysicsCategories.Border
//        normalBullet.physicsBody?.contactTestBitMask = PhysicsCategories.EnemyNormal | PhysicsCategories.EnemyMedium | PhysicsCategories.EnemyHard | PhysicsCategories.Border
//        self.addChild(normalBullet)
//
//        let bulletForceVector = CGVector(dx: 20 * cos(findAngle() + CGFloat.pi/2), dy: 20 * sin(findAngle() + CGFloat.pi/2))
//        normalBullet.physicsBody?.applyImpulse(bulletForceVector)
//
//
//
//        if manaBarLenght > 0 {
//            manaBar[manaBarLenght-1].isHidden = true
//            manaBarLenght -= 1
//        }
//    }
//
//
//    func fireMediumBullet(){
//
//        let mediumBullet = MediumBullet(imageNamed: "PlasmaBullet")
//        playSound(sound: mediumBulletSound)
//        mediumBullet.position = CGPoint(x: cannon.position.x , y: cannon.position.y )
//        mediumBullet.size = CGSize(width: 28, height: 40)
//        mediumBullet.physicsBody = SKPhysicsBody(circleOfRadius: mediumBullet.size.width/2)
//        mediumBullet.physicsBody?.affectedByGravity = false
//        mediumBullet.physicsBody?.restitution = 0
//        mediumBullet.zRotation = findAngle()
//        mediumBullet.zPosition = 1
//        mediumBullet.physicsBody?.angularDamping = 0
//        mediumBullet.physicsBody?.linearDamping = 0
//        mediumBullet.physicsBody?.allowsRotation = false
//        mediumBullet.physicsBody?.usesPreciseCollisionDetection = true
//        mediumBullet.physicsBody?.categoryBitMask = PhysicsCategories.MediumBullet
//        mediumBullet.physicsBody?.collisionBitMask = PhysicsCategories.Border
//        mediumBullet.physicsBody?.contactTestBitMask = PhysicsCategories.EnemyNormal | PhysicsCategories.EnemyMedium | PhysicsCategories.EnemyHard | PhysicsCategories.Border
//        self.addChild(mediumBullet)
//
//        let bulletForceVector = CGVector(dx: 20 * cos(findAngle() + CGFloat.pi/2), dy: 20 * sin(findAngle() + CGFloat.pi/2))
//        mediumBullet.physicsBody?.applyImpulse(bulletForceVector)
//
//        if manaBarLenght > 0 {
//            manaBar[manaBarLenght-1].isHidden = true
//            manaBar[manaBarLenght - 2].isHidden = true
//            manaBarLenght -= 2
//        }
//
//
//    }
    
    
    
    
    
    
    func spawnDamage(spawnPosition: CGPoint, damageValue: Int){
        
        let damage = SKLabelNode(fontNamed: "SPACEBOY")
        damage.text = String(damageValue)
        damage.fontSize = 15
        damage.setScale(0)
        damage.position = spawnPosition
        damage.fontColor = SKColor.red
        damage.zPosition = 100
        self.addChild(damage)
        
        let scaleIn = SKAction.scale(to: 4, duration: 0.5)
        let fadeOut = SKAction.fadeOut(withDuration: 0.3)
        let delete = SKAction.removeFromParent()
        
        let damageSequence = SKAction.sequence([scaleIn,fadeOut,delete])
        
        damage.run(damageSequence)
    }
    
    
    
    func freezeGame(){
        
        
        for i in 0...self.children.count{
            
            self.enumerateChildNodes(withName: String(i)){
                
                normalEnemy, stop in
                normalEnemy.removeAllActions()
            }
            
            self.enumerateChildNodes(withName: String(i)){
                
                mediumEnemy, stop in
                mediumEnemy.removeAllActions()
                
            }
            
            self.enumerateChildNodes(withName: String(i)){
                
                hardEnemy, stop in
                hardEnemy.removeAllActions()
                
            }
        }
    }
    
    
 
        
        
        
    
    
    func runGameOver(){
        
        currentGameState = GameState.afterGame
        
      
            freezeGame()
            let changeSceneAction = SKAction.run(changeGameOverScene)
            let waitToChangeScene = SKAction.wait(forDuration: 1)
            let changeSceneSequence = SKAction.sequence([waitToChangeScene,changeSceneAction])
            self.run(changeSceneSequence)
        }
    


    func changeGameOverScene(){
        
        let sceneToMoveTo = GameOverScene(size: self.size)
        sceneToMoveTo.scaleMode = self.scaleMode
        let myTransition = SKTransition.fade(withDuration: 0.5)
        self.view!.presentScene(sceneToMoveTo, transition: myTransition)
        
    }
    
    
    
    func findAngle() -> CGFloat {
        
        var angolo: CGFloat = 0.0
        let radians = atan2(fingerLocation.x - cannon.position.x , fingerLocation.y - cannon.position.y)
        
        if fingerLocation.y > cannon.position.y {
            angolo = -radians
            
            if angolo >= 1.099 {
                angolo = 1.099
            }
            if angolo <= -1.099{
                angolo = -1.099
            }
            directTouch = true
        }
        else { directTouch = false
            lineGuide.isHidden = true
        }
        return angolo
    }
    
    func shakeCamera(canvas: SKSpriteNode, duration:Float) {
        
        let amplitudeX:Float = 10;
        let amplitudeY:Float = 6;
        let numberOfShakes = duration / 0.04;
        var actionsArray: [SKAction] = [SKAction]()
        
        for _ in 1...Int(numberOfShakes) {
            // build a new random shake and add it to the list
            let moveX = Float(arc4random_uniform(UInt32(amplitudeX))) - amplitudeX / 2;
            let moveY = Float(arc4random_uniform(UInt32(amplitudeY))) - amplitudeY / 2;
            let shakeAction = SKAction.moveBy(x: CGFloat(moveX), y: CGFloat(moveY), duration: 0.02);
            shakeAction.timingMode = SKActionTimingMode.easeOut;
            actionsArray.append(shakeAction);
            actionsArray.append(shakeAction.reversed());
        }
        
        let actionSeq = SKAction.sequence(actionsArray);
        canvas.run(actionSeq);
    }
    
    
    
    
 
    var maxLife = 3
    
    func removeHealthPoint(){
        
        let fadeOut = SKAction.fadeOut(withDuration: 1)
        
        if maxLife > 1 {
            healthBar[maxLife-1].run(fadeOut)
            maxLife -= 1
        }else if maxLife == 1{
            healthBar[maxLife-1].run(fadeOut)
            maxLife -= 1
            runGameOver()
            playSound(sound: SKAction.playSoundFileNamed("gameOverSound", waitForCompletion: false))
            
        }
        }
    
    func addHealthPoint(){
        
        
        let fadeIn = SKAction.fadeIn(withDuration: 1)
        
        if maxLife < 3 {
            
            healthBar[maxLife].run(fadeIn)
            maxLife += 1
            
            playSound(sound: recoverLifeSound)
            
            print("vita aggiunta, maxlife: \(maxLife)")
        }
        }
    
    
    let fadeIn = SKAction.fadeIn(withDuration: 1)
    
    
    
    func addManaPoints(numbers: Int){
        
        var manaBarLenght = cannonControllerReference?.manaBarLenght
        
        print("Aggiungo Mana points")
        
        print("ManaLiunghezza = \(manaBarLenght!)")
        
        if manaBarLenght == 9 {
            
            let barPiece = manaBar[manaBarLenght! + 1]
            barPiece.run(fadeIn)
           
            manaBarLenght = manaBarLenght! + 1
             print("Aggiunto 1 Ammo")
        }
        else if manaBarLenght == 8 {
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
      
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 2
            print("Aggiunti 2 Ammo")
            
            
        }else if manaBarLenght! == 7{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
      
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
             print("Aggiunti 3 Ammo")
            
        }
        else if manaBarLenght! == 6{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }
        else if manaBarLenght! == 5{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }
        else if manaBarLenght! == 4{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }
        else if manaBarLenght! == 3{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }
        else if manaBarLenght! == 3{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }else if manaBarLenght! == 2{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }
        else if manaBarLenght! == 1{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }else if manaBarLenght! == 0{
            
            print("Mana < 7")
            
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            barPiece3.run(fadeIn)
            
            manaBarLenght = manaBarLenght! + 3
            print("Aggiunti 3 Ammo")
            
        }
         playSound(sound: recoverLifeSound)
        }
    
    
    var lifeCountForSpawning = 0
    var nebulaSpawning : Int = 0
    var addOnManaCount : Int = 0
    
    func searchSpawningPosition(){
        
        let numberOfSpawn = Int.random(in: 1 ... 3)
        
        addOnManaCount += numberOfSpawn
        
        if currentGameState == .inGame{
        nebulaSpawning = Int.random(in: 1 ... 6)
        }
        
        if nebulaSpawning == 5{
            let nebulaSpawningPosition = Int.random(in: 400...600 )
            nebublaSpawn(inPosition: CGPoint(x: 202, y: nebulaSpawningPosition))
            
        }
        
        print("\(numberOfSpawn) posizioni da spawnare\n")
        
        var spawningPositionCopy = spawningPositions
        
        for _ in 0...numberOfSpawn - 1 {
            
            //Scelgo una posizione a caso
            let positionOfSpawn = spawningPositionCopy.randomElement()!
            
            //scelgo il tipo di nemico da spawnare per fare debug impostare intervallo tra 4-5
            let enemySpawnType = Int.random(in: 1 ..< 6)
            
           
            if enemySpawnType == 4{
                
                lifeCountForSpawning += 1
            }
            else if enemySpawnType == 5{
                addOnManaCount += 1
            }
            
            //se il conteggio è arrivato a 5, e l'utente ha perso almeno una vita, fai spawnare la vita, se no spawna un nemico
                if lifeCountForSpawning > 5 && maxLife < 3{
                    
                    print("Raggiunto il massimo, vita da spawnare")
                    let healthObject = enemySpawn(enemyType: EnemyType.health, position: positionOfSpawn)
                    
             
                    healthIndex += 1
                    healthObject.name = "Health\(healthIndex)"
                    healthObject.run(SKAction.scale(to: 0.40, duration: 3))
                    lifeCountForSpawning = 0
                    
                    
                }else if  addOnManaCount > 5{
                    
                    
                    print("Spawning AddonMana")
                    let manaObject = enemySpawn(enemyType: EnemyType.addOnMana, position: positionOfSpawn)
                    addOnManaIndex += 1
                    manaObject.run(SKAction.scale(to: 0.40, duration: 3))
                    manaObject.name = "Mana\(addOnManaIndex)"
                    addOnManaCount = 0
                    
                }
//                else{
//
//                    print("Nemico \(i) spawnato di tipo: \(enemySpawnType - numberOfSpawn)")
//
//                    enemySpawn(enemyType: EnemyType(rawValue: (enemySpawnType - numberOfSpawn))!, position: positionOfSpawn)
//                    lifeCountForSpawning += 1
//                    print("Livello caricamento vita: \(lifeCountForSpawning)\n")
//                    print(maxLife)
//                }
                else{
                
                    let newSpawn = Int.random(in: 1 ... 3)
                    let enemy = enemySpawn(enemyType: EnemyType(rawValue: newSpawn)!, position: positionOfSpawn)
                    
                    enemyIndex += 1
                    enemy.name = "\(enemyIndex)"
                
            }
            
            let index = spawningPositionCopy.firstIndex(of: positionOfSpawn)!
            spawningPositionCopy.remove(at: index)
            
    }
    }
    
   
    
    func changeVolume(audioNode: SKAudioNode, withVolume: Float){
        
        let changeVolume = SKAction.changeVolume(to: withVolume, duration: 0.0)
        audioNode.run(changeVolume)
    }
    
    func playSound(sound : SKAction ){
        
        run(sound)
        
    }
    
    func addScore(amount: Int){
        
        gameScore = gameScore + amount
        scoreLabel.text = "\(gameScore)"
        
        
    }
    
    func addManaRecharge(quantity: Int){
        
        manaRechargeValue += quantity
        manaRechargeLabel.text = "\(manaRechargeValue)"
        
    }
    
    
    func explosion(inPosition: CGPoint, emitterName: String){
        
       
            let explosion = SKEmitterNode(fileNamed: emitterName)!
            explosion.zPosition = 10
            explosion.position = inPosition
        
            self.addChild(explosion)
        
        self.run(SKAction.wait(forDuration: 0.2)) {
            explosion.removeFromParent()
        }
            
        
    }
    
    func nebublaSpawn(inPosition: CGPoint){
        
       
        let nebula = SKEmitterNode(fileNamed: "Fog")!
        nebula.zPosition = 5
        nebula.position = inPosition
        nebula.alpha = 0
        
        self.addChild(nebula)
        
        let fadeIn = SKAction.fadeIn(withDuration: 2)
        let wait = SKAction.wait(forDuration: 10)
        let fadeOut = SKAction.fadeOut(withDuration: 2)
        
        let nebulaSequence = SKAction.sequence([fadeIn,wait,fadeOut])
        
        nebula.run(nebulaSequence)
        
        }
    
    
    func initGameCenter() {
        
        // Check if user is already authenticated in game center
        if GKLocalPlayer.local.isAuthenticated == false {
            
            // Show the Login Prompt for Game Center
            GKLocalPlayer.local.authenticateHandler = {(viewController, error) -> Void in
                if viewController != nil {
//                    self.scene!.isPaused = true
//                    self.present(viewController!, animated: true, completion: nil)
                    
                    // Add an observer which calls ‚gameCenterStateChanged‘ to handle a changed game center state
                    let notificationCenter = NotificationCenter.default
                    notificationCenter.addObserver(self, selector: Selector(("gameCenterStateChanged")), name:  NSNotification.Name(rawValue: "GKPlayerAuthenticationDidChangeNotificationName"), object: nil)
                }
            }
        }
        
    }
    
}


extension UIDevice {
    static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }
}


public struct PhysicsCategories{
    
    static let None : UInt32 = 0
    
    static let NormalBullet : UInt32 = 0b0001
    static let MediumBullet : UInt32 = 0b0010
    static let HardBullet : UInt32 = 0b0011
    
    static let EnemyNormal : UInt32 = 0b0100
    static let EnemyMedium : UInt32 = 0b0101
    static let EnemyHard : UInt32 = 0b0110
    
    
    static let SpaceShip : UInt32 = 0b0111
    static let PowerUp : UInt32 = 0b1100
    
    static let Border : UInt32 = 0b1000
}

var mana: Int = 0
let manaTexture = SKTexture(imageNamed: "manaUnlock")
let healthTexture = SKTexture(imageNamed: "healthUnlock")

extension GameScene: SKPhysicsContactDelegate {
    
    
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        
        var body1 = SKPhysicsBody()
        var body2 = SKPhysicsBody()
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            
            body1 = contact.bodyA
            body2 = contact.bodyB
            
            
        }else{
            
            body1 = contact.bodyB
            body2 = contact.bodyA
         
        }
        
        
        
        /**************BORDER REFLECTION************/
        
        if (body1.categoryBitMask == PhysicsCategories.NormalBullet || body1.categoryBitMask == PhysicsCategories.MediumBullet || body1.categoryBitMask == PhysicsCategories.HardBullet ) && body2.categoryBitMask == PhysicsCategories.Border {
            
            let borderName = String(body2.node!.name!)
            let bulletCollisionPosition = body1.node?.position
            
            if borderName == "leftBorder"{
                
                explosion(inPosition: CGPoint(x: bulletCollisionPosition!.x, y: bulletCollisionPosition!.y), emitterName: "CollisionLeft")
                
            }else if borderName == "rightBorder"{
                
                explosion(inPosition: CGPoint(x: bulletCollisionPosition!.x, y: bulletCollisionPosition!.y), emitterName: "CollisionRight")
                
            }else if borderName == "topBorder"{
                body1.node?.removeFromParent()
                
            }
            
            
            if body1.node?.zRotation != nil {
                
                let node = body1.node
                let move = SKAction.rotate(byAngle: -2 * node!.zRotation, duration: 0)
                node?.run(move)
            }
        }
        
        if (body1.categoryBitMask == PhysicsCategories.NormalBullet || body1.categoryBitMask == PhysicsCategories.MediumBullet || body1.categoryBitMask == PhysicsCategories.HardBullet)  && body2.categoryBitMask == PhysicsCategories.PowerUp{
            
            
            print("Collisione con powerUp")
              if body2.node?.name != nil {
            
            let nodeName = String(body2.node!.name!)
            let node = childNode(withName: nodeName) as! Enemy
            print("Il nodo poweup si chiama: \(nodeName)")
            
            let position = body2.node?.position
            
            
            if node.health == 0 && node.texture != manaTexture{
                node.health = 1
                node.changeTexture(texture: manaTexture)
                mana = node.moveDown(position: CGPoint(x: position!.x, y: position!.y - 600))
              
                playSound(sound: recoverLifeSound)
                addManaRecharge(quantity: mana)
                
              
                
            }
            else if node.health == -1 && node.texture != healthTexture{
                node.health = 1
                node.changeTexture(texture: healthTexture)
                mana = node.moveDown(position: CGPoint(x: position!.x, y: position!.y - 800))
               addHealthPoint()
                playSound(sound: recoverLifeSound)
                
              
                
            }
                
                
                if body1.categoryBitMask == PhysicsCategories.HardBullet{
                explosion(inPosition: body1.node!.position, emitterName: "GaussExplosion")
                playSound(sound: explosionSound)
                    
                    for nodeChild in scene!.children{
                        
                        let dx = node.position.x - nodeChild.position.x
                        let dy = node.position.y - nodeChild.position.y
                        
                        let distance = sqrt(dx*dx + dy*dy)
                        
                        if (distance <= 150) {
                            
                            if (nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyNormal) {
                                gameScore += 1
                                scoreLabel.text = "\(gameScore)"
                                explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                                nodeChild.removeFromParent()
                            }
                            else if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyMedium {
                                gameScore += 2
                                scoreLabel.text = "\(gameScore)"
                                explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                                nodeChild.removeFromParent()
                            }
                            else if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyHard {
                                gameScore += 3
                                scoreLabel.text = "\(gameScore)"
                                explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                                nodeChild.removeFromParent()
                            }
                            else if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.PowerUp {
                              
                             explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                                
                            }
                            
                        }
                    }
                    
                }else{
            explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                    playSound(sound: enemyHitSound)
                }
                
                
                
                
               
            
            
            node.removeAllChildren()
            body1.node?.removeFromParent()
            
            
        }
        
        }
        if (body1.categoryBitMask == PhysicsCategories.NormalBullet || body1.categoryBitMask == PhysicsCategories.MediumBullet)  && body2.categoryBitMask == PhysicsCategories.EnemyNormal{
            
            playSound(sound: normalEnemySound)
            addScore(amount: 1)
            
            playSound(sound: explosionSound)
            explosion(inPosition: body2.node!.position, emitterName: "Explosion")
          
            body1.node?.removeFromParent()
            body2.node?.removeFromParent()
            }
        
        
        
        /*****************+IF A BULLET HIT A ENEMY*/
        
        if body1.categoryBitMask == PhysicsCategories.NormalBullet && (body2.categoryBitMask == PhysicsCategories.EnemyMedium || body2.categoryBitMask == PhysicsCategories.EnemyHard){
            
            if body2.node?.name != nil && body1.node?.position != nil {
            let nodeName = String(body2.node!.name!)
            let node = childNode(withName: nodeName) as! Enemy
            let position = body1.node?.position
            
            node.gotHit(1)
            playSound(sound: enemyHitSound)
            print("Explosion in position: \(position)")
            explosion(inPosition: position!, emitterName: "CollisionEnemy")
            
            
            if node.isDead() {
                
                playSound(sound: explosionSound)
                explosion(inPosition: node.position, emitterName: "Explosion")
                
                if body2.categoryBitMask == PhysicsCategories.EnemyMedium {
                    
                   addScore(amount: 2)
                 }
                else {
                  
                    addScore(amount: 3)
                }
                body2.node?.removeFromParent()
            }
            body1.node?.removeFromParent()
            
            
            }
        }else if body1.categoryBitMask == PhysicsCategories.MediumBullet && (body2.categoryBitMask == PhysicsCategories.EnemyMedium || body2.categoryBitMask == PhysicsCategories.EnemyHard)  {
            
            if body2.node?.name != nil {
            let nodeName = String(body2.node!.name!)
            let node = childNode(withName: nodeName) as! Enemy
            
            node.gotHit(3)
            playSound(sound: enemyHitSound)
            explosion(inPosition: body1.node!.position , emitterName: "CollisionEnemy")
           
            
            if node.isDead() {
                
                playSound(sound: explosionSound)
                explosion(inPosition: node.position, emitterName: "Explosion")
                
                if body2.categoryBitMask == PhysicsCategories.EnemyMedium {
               
                    addScore(amount: 2)
                }
                else {
                
                    addScore(amount: 3)
                }
                
                body2.node?.removeFromParent()
            }
            body1.node?.removeFromParent()
            
        }
        }
        else if body1.categoryBitMask == PhysicsCategories.HardBullet && (body2.categoryBitMask == PhysicsCategories.EnemyNormal || body2.categoryBitMask == PhysicsCategories.EnemyMedium || body2.categoryBitMask == PhysicsCategories.EnemyHard || body2.categoryBitMask == PhysicsCategories.EnemyHard)  {
            
            if body1.node?.name != nil {
                
                let nodeName = String(body1.node!.name!)
                let node = childNode(withName: nodeName) as! SKSpriteNode
                
                explosion(inPosition: body1.node!.position, emitterName: "GaussExplosion")
                playSound(sound: explosionSound)
                
                print(node.position)
                
                for nodeChild in scene!.children{
                    
                    let dx = node.position.x - nodeChild.position.x
                    let dy = node.position.y - nodeChild.position.y
                    
                    let distance = sqrt(dx*dx + dy*dy)
                    
                    if (distance <= 150) {
                        
                        if (nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyNormal) {
                            gameScore += 1
                            scoreLabel.text = "\(gameScore)"
                            explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                            nodeChild.removeFromParent()
                        }
                        else if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyMedium {
                            gameScore += 2
                            scoreLabel.text = "\(gameScore)"
                            explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                            nodeChild.removeFromParent()
                        }
                        else if nodeChild.physicsBody?.categoryBitMask == PhysicsCategories.EnemyHard {
                            gameScore += 3
                            scoreLabel.text = "\(gameScore)"
                            explosion(inPosition: body2.node!.position, emitterName: "Explosion")
                            nodeChild.removeFromParent()
                        }
                
                            
                        }

                    
                }
                node.removeFromParent()
            }
            
        }
        
        
        
        /*****************IF A ENEMY HIT THE SPACESHIP AMMO FRAME*****************/
        
        if (body1.categoryBitMask == PhysicsCategories.EnemyNormal || body1.categoryBitMask == PhysicsCategories.EnemyMedium || body1.categoryBitMask == PhysicsCategories.EnemyHard) && body2.categoryBitMask == PhysicsCategories.SpaceShip{
            
            UIDevice.vibrate()
            print("Contatto nemico macchina del tempo")
            removeHealthPoint()
           // print(body1.node?.name! as A)
            body1.node?.removeFromParent()
            shakeCamera(canvas: spaceShip, duration: 0.3)
            
//        }else if body1.categoryBitMask == PhysicsCategories.SpaceShip  && body2.categoryBitMask == PhysicsCategories.PowerUp{
//
//            print("Collisione SpaceShip - PowerUp")
//
//            let nodeName = String(body2.node!.name!)
//
//            let node = childNode(withName: nodeName) as! Enemy
//
//
//
//            if node.health == 0{
//
//               addManaRecharge()
//
//                }
//            else if node.health == -1 {
//
//               addHealthPoint()
//
//            }
     }
      
    }
    

}

