//
//  CannonController.swift
//  FrolliniGame
//
//  Created by Aurelio Aliberti on 12/06/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//

import Foundation
import SpriteKit

var normalBulletSound = SKAction.playSoundFileNamed("laser1", waitForCompletion: false)
var mediumBulletSound = SKAction.playSoundFileNamed("plasmaSound.wav", waitForCompletion: false)
var hardBulletSound = SKAction.playSoundFileNamed("gaussCannonSound.wav", waitForCompletion: false)
var emptyMana = SKAction.playSoundFileNamed("emptyMana", waitForCompletion: false)
var chargingMana = SKAction.playSoundFileNamed("manaCharging", waitForCompletion: false)

class CannonController : SKSpriteNode{
    

    public var manaBarLenght : Int?
        
    var sceneCopy : SKScene?
    var cannon: SKSpriteNode?
    var manaBar = [SKSpriteNode]()
    var hardBulletNumber = 0
    var gameState = -1
    
    
    
   
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
        self.isUserInteractionEnabled = true
    }

    convenience init(textureName: String, cannon: SKSpriteNode, manaBarLenght: Int, manaBar: [SKSpriteNode]) {
        let texture = SKTexture(imageNamed: textureName)
        self.init(texture: texture, color: SKColor.clear, size: texture.size())
        self.cannon = cannon
        self.manaBarLenght = manaBarLenght
        self.manaBar = manaBar
        
      
        }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
 
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if gameState == 0{
        
    
       if weaponSelected == 1 {
            
            if manaBarLenght! > 0 {
                fireNormalBullet()
                
            }else {
                noAmmoLabelAlarm()
                playSound(sound: emptyMana)
            }
        }
        else if weaponSelected == 2{
            
            if manaBarLenght! > 1 {
                fireMediumBullet()
                
            }else{
                noAmmoLabelAlarm()
                playSound(sound: emptyMana)
            }
        }
        else if weaponSelected == 3{
            
            if manaBarLenght! > 4 {
                fireHardBullet()
                
            }else {
                noAmmoLabelAlarm()}
                playSound(sound: emptyMana)
            
            
        }
        }
    }
    
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if gameState == 0{
            
       for touch in touches{

            //Controller movement
            let pointOfTouch = touch.location(in: self)
            let previousPointOfTouch = touch.previousLocation(in: self)
            
            let amountDragged = pointOfTouch.x - previousPointOfTouch.x
            
            self.position.x += amountDragged
            
            //How the player stay in the gameArea
            if self.position.x > 270{
                self.position.x = 270
                
            }
            if self.position.x < 150{
                self.position.x = 150
            }
            
            cannon?.zRotation = cannonRotation()
        }
        }
        
    }
    
    func startManaRecovery() {
        
        let fadeInAction = SKAction.fadeIn(withDuration: 1)
        
        let wait1Second = SKAction.wait(forDuration: 1)
        // 2 increment action
        let incrementCounter = SKAction.run { [weak self] in
            if self!.manaBarLenght! < 10 {
                self!.manaBarLenght! += 1
                
                var barPiece : SKSpriteNode
                barPiece = self!.manaBar[(self!.manaBarLenght! - 1)]
                barPiece.run(fadeInAction)
            }
        }
        // 3. wait + increment
        let sequence = SKAction.sequence([wait1Second, incrementCounter])
        // 4. (wait + increment) forever
        let repeatForever = SKAction.repeatForever(sequence)
        
        // run it!
        self.run(repeatForever)
    }
    
    
    func addMana(){
        
        
        
//
//        if manaBarLenght! < 8 && manaBarLenght! > 2{
//
//           let barPiece = manaBar[manaBarLenght! ]
//            let barPiece2 = manaBar[manaBarLenght! + 1]
//            let barPiece3 = manaBar[manaBarLenght! + 2]
//
//
//            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
//
//            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
//            let wait = SKAction.wait(forDuration: 0.2)
//            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
//            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
//            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
//
//            manaBar[4].run(fadeIn)
//            manaBar[5].run(fadeIn)
//            manaBar[6].run(fadeIn)
//            manaBar[manaBarLenght!-3].run(fadeIn)
//            manaBar[manaBarLenght!-2].run(fadeIn)
//            manaBar[manaBarLenght!-1].run(fadeIn)
//            barPiece.run(chargeManaSequence)
//            barPiece2.run(chargeManaSequence)
//            barPiece3.run(chargeManaSequence)
//
//
//            manaBarLenght! += 3
//            manaRechargeValue -= 1
//
//        }
         if manaBarLenght == 0{
            
           
            let barPiece = manaBar[manaBarLenght!]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            
            manaBar[0].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
        }
            
         else if manaBarLenght == 1 {
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            
            manaBar[0].run(fadeIn)
            manaBar[1].run(fadeIn)
            //manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
         }
         else if manaBarLenght == 2{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[0].run(fadeIn)
            manaBar[1].run(fadeIn)
            manaBar[2].run(fadeIn)
            manaBar[3].run(fadeIn)
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
         }
            
            
            
            
        else if manaBarLenght == 3{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[1].run(fadeIn)
            manaBar[2].run(fadeIn)
            manaBar[3].run(fadeIn)
             manaBar[manaBarLenght!-3].run(fadeIn)
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
        }
         else if manaBarLenght == 4{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[2].run(fadeIn)
            manaBar[3].run(fadeIn)
             manaBar[4].run(fadeIn)
            
            manaBar[manaBarLenght!-3].run(fadeIn)
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
         }   else if manaBarLenght == 5{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[2].run(fadeIn)
            manaBar[3].run(fadeIn)
            manaBar[4].run(fadeIn)
            manaBar[5].run(fadeIn)
            manaBar[6].run(fadeIn)
            
            manaBar[manaBarLenght!-3].run(fadeIn)
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
         }else if manaBarLenght == 6{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[3].run(fadeIn)
            manaBar[4].run(fadeIn)
           
            
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
         }else if manaBarLenght == 7{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[4].run(fadeIn)
            manaBar[5].run(fadeIn)
            
            
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
         }
            
         else if manaBarLenght == 7{
            
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
            let barPiece3 = manaBar[manaBarLenght! + 2]
            
            
            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
            
            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
            let wait = SKAction.wait(forDuration: 0.2)
            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
            
            manaBar[4].run(fadeIn)
            manaBar[5].run(fadeIn)
            manaBar[6].run(fadeIn)
            
            
            manaBar[manaBarLenght!-2].run(fadeIn)
            manaBar[manaBarLenght!-1].run(fadeIn)
            barPiece.run(chargeManaSequence)
            barPiece2.run(chargeManaSequence)
            barPiece3.run(chargeManaSequence)
            
            
            manaBarLenght! += 3
            manaRechargeValue -= 1
            
            
            
            
         }
            
//      else if manaBarLenght == 1 {
//
//            let barPiece = manaBar[manaBarLenght! ]
//            let barPiece2 = manaBar[manaBarLenght! + 1]
//            let barPiece3 = manaBar[manaBarLenght! + 2]
//
//
//            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
//
//            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
//            let wait = SKAction.wait(forDuration: 0.2)
//            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
//            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
//            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
//
//
//            manaBar[0].run(fadeIn)
//            manaBar[1].run(fadeIn)
//            manaBar[manaBarLenght!-1].run(fadeIn)
//            barPiece.run(chargeManaSequence)
//            barPiece2.run(chargeManaSequence)
//            barPiece3.run(chargeManaSequence)
//
//
//            manaBarLenght! += 3
//            manaRechargeValue -= 1
//
//
//        }
//
//        else if manaBarLenght == 3 {
//
//            let barPiece = manaBar[manaBarLenght! ]
//            let barPiece2 = manaBar[manaBarLenght! + 1]
//            let barPiece3 = manaBar[manaBarLenght! + 2]
//
//
//            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
//
//            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
//            let wait = SKAction.wait(forDuration: 0.2)
//            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
//            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
//            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
//
//
//            manaBar[0].run(fadeIn)
//            manaBar[1].run(fadeIn)
//            manaBar[2].run(fadeIn)
//            manaBar[manaBarLenght!-1].run(fadeIn)
//            barPiece.run(chargeManaSequence)
//            barPiece2.run(chargeManaSequence)
//            barPiece3.run(chargeManaSequence)
//
//
//            manaBarLenght! += 3
//            manaRechargeValue -= 1
//
//
//        }
//        else if manaBarLenght == 0{
//
//            let barPiece4 = manaBar[0]
//            let barPiece = manaBar[manaBarLenght!]
//            let barPiece2 = manaBar[manaBarLenght! + 1]
//            let barPiece3 = manaBar[manaBarLenght! + 2]
//
//
//
//            let greenColor = UIColor(displayP3Red: 168, green: 220, blue: 84, alpha: 1)
//
//            let fadeIn = SKAction.fadeIn(withDuration: 0.1)
//            let wait = SKAction.wait(forDuration: 0.2)
//            let changetoRed = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 0.5)
//            let changeToGreen = SKAction.colorize(with: greenColor, colorBlendFactor: 1, duration: 0.5)
//            let chargeManaSequence = SKAction.sequence([wait,changetoRed,fadeIn,changeToGreen])
//
//
//            barPiece4.run(chargeManaSequence)
//            barPiece.run(chargeManaSequence)
//            barPiece2.run(chargeManaSequence)
//            barPiece3.run(chargeManaSequence)
//
//            
//
//            manaBarLenght! += 3
//            manaRechargeValue -= 1
//
//        }
        else if manaBarLenght == 8 {
            let barPiece = manaBar[manaBarLenght! ]
            let barPiece2 = manaBar[manaBarLenght! + 1]
           
            
            let fadeIn = SKAction.fadeIn(withDuration: 1)
            barPiece.run(fadeIn)
            barPiece2.run(fadeIn)
            
            manaBar[5].run(fadeIn)
            manaBar[6].run(fadeIn)
            manaBar[7].run(fadeIn)
            
            manaBarLenght! += 2
             manaRechargeValue -= 1
        }
        else if manaBarLenght == 9 {
            
                let barPiece = manaBar[manaBarLenght! ]
               
                let fadeIn = SKAction.fadeIn(withDuration: 1)
                barPiece.run(fadeIn)
            
            
            manaBar[7].run(fadeIn)
            manaBar[8].run(fadeIn)
                manaBarLenght! += 1
                manaRechargeValue -= 1
        }
    }
    

    func cannonRotation() -> CGFloat{
        var angle: CGFloat = 0.0
        //Cannon zRotation following the controller movement
        var radians = atan2(self.position.x - cannon!.position.x ,  self.position.y - cannon!.position.y)
        
        if radians <= -CGFloat.pi/2 {
            angle = -radians - CGFloat.pi
        }
        if radians >= CGFloat.pi/2 {
            angle = -radians + CGFloat.pi
        }
        
        if self.position.x >= 270 {
            radians = 1
        }
        if self.position.x <= 150{
            radians = -1
            
        }
        
        return angle
        
    }
    
    func playSound(sound : SKAction)
    {
        run(sound)
    }
    
    func fireNormalBullet(){
        
        if manaBarLenght! >  0 {
            
        let normalBullet = NormalBullet(imageNamed: "LaserBullet")
        playSound(sound: normalBulletSound)
        normalBullet.position = CGPoint(x:  (cannon?.frame.midX)!, y: (cannon?.frame.midY)!)
        normalBullet.size = CGSize(width: 8, height: 35)
        normalBullet.physicsBody = SKPhysicsBody(rectangleOf: normalBullet.size)
        normalBullet.physicsBody?.affectedByGravity = false
        normalBullet.zPosition = 1
        normalBullet.zRotation = cannonRotation()
        normalBullet.physicsBody?.restitution = 0
        normalBullet.physicsBody?.friction = 0
        normalBullet.physicsBody?.angularDamping = 0
        normalBullet.physicsBody?.linearDamping = 0
        normalBullet.physicsBody?.allowsRotation = false
        normalBullet.physicsBody?.usesPreciseCollisionDetection = true
        normalBullet.physicsBody?.categoryBitMask = PhysicsCategories.NormalBullet
        normalBullet.physicsBody?.collisionBitMask = PhysicsCategories.Border
        normalBullet.physicsBody?.contactTestBitMask = PhysicsCategories.EnemyNormal | PhysicsCategories.EnemyMedium | PhysicsCategories.EnemyHard | PhysicsCategories.Border
        scene!.addChild(normalBullet)
        
        
        let bulletForceVector = CGVector(dx:  5 * cos(cannonRotation() + CGFloat.pi/2), dy: 5 * sin(cannonRotation() + CGFloat.pi/2))
        normalBullet.physicsBody?.applyImpulse(bulletForceVector)
        
            let barPiece = manaBar[manaBarLenght!-1]
            let fadeOut = SKAction.fadeOut(withDuration: 1)
            barPiece.run(fadeOut)
            manaBarLenght! -= 1
            
        }
    }
    
    
    
    func fireMediumBullet(){

       let mediumBullet = NormalBullet(imageNamed: "PlasmaBullet")
        playSound(sound: mediumBulletSound)
        mediumBullet.position = CGPoint(x:  (cannon?.frame.midX)!, y: (cannon?.frame.midY)!)
        mediumBullet.physicsBody = SKPhysicsBody(rectangleOf: mediumBullet.size)
        mediumBullet.physicsBody?.affectedByGravity = false
        mediumBullet.zPosition = 1
        mediumBullet.zRotation = cannonRotation()
        mediumBullet.physicsBody?.restitution = 0
        mediumBullet.physicsBody?.friction = 0
        mediumBullet.physicsBody?.angularDamping = 0
        mediumBullet.physicsBody?.linearDamping = 0
        mediumBullet.physicsBody?.allowsRotation = false
        mediumBullet.physicsBody?.usesPreciseCollisionDetection = true
        mediumBullet.physicsBody?.categoryBitMask = PhysicsCategories.MediumBullet
        mediumBullet.physicsBody?.collisionBitMask = PhysicsCategories.Border
        mediumBullet.physicsBody?.contactTestBitMask = PhysicsCategories.EnemyNormal | PhysicsCategories.EnemyMedium | PhysicsCategories.EnemyHard | PhysicsCategories.Border
        
        scene!.addChild(mediumBullet)
        
        let bulletForceVector = CGVector(dx: 10 * cos(cannonRotation() + CGFloat.pi/2), dy: 10 * sin(cannonRotation() + CGFloat.pi/2))
        mediumBullet.physicsBody?.applyImpulse(bulletForceVector)
        
        
        if manaBarLenght! > 1 {
            
            let barPiece = manaBar[manaBarLenght!-1]
            let barPiece2 = manaBar[manaBarLenght! - 2]
            
            let fadeOut = SKAction.fadeOut(withDuration: 1)
            barPiece.run(fadeOut)
            barPiece2.run(fadeOut)
            manaBarLenght! -= 2
        }
    }
    
    
    func fireHardBullet(){
        
        let hardBullet = NormalBullet(imageNamed: "GaussBullet")
        playSound(sound: hardBulletSound)
        hardBullet.name = "hardBullet" + String(hardBulletNumber)
        hardBullet.position = CGPoint(x:  (cannon?.frame.midX)!, y: (cannon?.frame.midY)!)
        hardBullet.physicsBody = SKPhysicsBody(rectangleOf: hardBullet.size)
        hardBullet.physicsBody?.affectedByGravity = false
        hardBullet.zPosition = 1
        hardBullet.zRotation = cannonRotation()
        hardBullet.physicsBody?.restitution = 0
        hardBullet.physicsBody?.friction = 0
        hardBullet.physicsBody?.angularDamping = 0
        hardBullet.physicsBody?.linearDamping = 0
        hardBullet.physicsBody?.allowsRotation = false
        hardBullet.physicsBody?.usesPreciseCollisionDetection = true
        hardBullet.physicsBody?.categoryBitMask = PhysicsCategories.HardBullet
        hardBullet.physicsBody?.collisionBitMask = PhysicsCategories.Border 
        hardBullet.physicsBody?.contactTestBitMask = PhysicsCategories.EnemyNormal | PhysicsCategories.EnemyMedium | PhysicsCategories.EnemyHard | PhysicsCategories.Border
        hardBulletNumber += 1
        scene!.addChild(hardBullet)
        
        
        if manaBarLenght ?? 1 > 4{
            
            let barPiece = manaBar[manaBarLenght! - 1]
            let barPiece2 = manaBar[manaBarLenght! - 2]
            let barPiece3 = manaBar[manaBarLenght! - 3]
            let barPiece4 = manaBar[manaBarLenght! - 4]
            let barPiece5 = manaBar[manaBarLenght! - 5]
            
            let fadeOut = SKAction.fadeOut(withDuration: 1)
            barPiece.run(fadeOut)
            barPiece2.run(fadeOut)
            barPiece3.run(fadeOut)
            barPiece4.run(fadeOut)
            barPiece5.run(fadeOut)
            manaBarLenght! -= 5
        
        
        let bulletForceVector = CGVector(dx: 30 * cos(cannonRotation() + CGFloat.pi/2), dy: 30 * sin(cannonRotation() + CGFloat.pi/2))
        hardBullet.physicsBody?.applyImpulse(bulletForceVector)
       
        }
    }
    
    func noAmmoLabelAlarm(){
        
            let noAmmoLabel = SKLabelNode(fontNamed: "SPACEBOY")
            
            noAmmoLabel.text = "Empty Mana!"
            noAmmoLabel.fontSize = 7
            
            noAmmoLabel.fontColor = .red
            noAmmoLabel.zPosition = 10
            noAmmoLabel.zRotation = 0.178
            noAmmoLabel.alpha = 0
            noAmmoLabel.position.y = 142
            noAmmoLabel.position.x = 55
            
            scene!.addChild(noAmmoLabel)
            
            let fadeInAction = SKAction.fadeIn(withDuration: 0.5)
            let fadeOutAction = SKAction.fadeOut(withDuration: 0.5)
            
            let scaleSequence = SKAction.sequence([fadeInAction,fadeOutAction])
            
            noAmmoLabel.run(scaleSequence)
        }
    
    
}
