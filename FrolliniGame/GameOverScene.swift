//
//  GameOverScene.swift
//  FrolliniGame
//
//  Created by Aurelio Aliberti on 30/05/2019.
//  Copyright © 2019 Frollini all'Amarena. All rights reserved.
//


import Foundation
import SpriteKit


class GameOverScene : SKScene{
    
    
    let restartLabel = SKLabelNode(fontNamed: "SPACEBOY")
    let quitLabel = SKLabelNode(fontNamed: "SPACEBOY")
    
    
    override func didMove(to view: SKView) {
        
        
        let background = SKSpriteNode(imageNamed: "backgroundUniverse")
        background.size = self.size
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        background.zPosition = 0
        self.addChild(background)
        
       let spaceshipTransiction = SKSpriteNode(imageNamed: "FrameNavicellaTransition")
        spaceshipTransiction.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        spaceshipTransiction.size.height = self.size.height
        spaceshipTransiction.size.width = self.size.width
        spaceshipTransiction.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        spaceshipTransiction.zPosition = 18
        self.addChild(spaceshipTransiction)
        
        let gameOverLabel = SKLabelNode(fontNamed: "SPACEBOY")
        gameOverLabel.text = "Game Over"
        gameOverLabel.fontSize = 40
        gameOverLabel.fontColor = SKColor(red: 191/255, green: 25/255, blue: 0/255, alpha: 1)
        gameOverLabel.position = CGPoint(x: self.size.width * 0.5, y: self.size.height * 0.75
        )
        gameOverLabel.zPosition = 1
        self.addChild(gameOverLabel)
        
        
        
        quitLabel.text = "Quit"
        quitLabel.fontSize = 30
        quitLabel.fontColor = SKColor(red: 191/255, green: 25/255, blue: 0/255, alpha: 1)
        quitLabel.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 6
        )
        quitLabel.zPosition = 2
        self.addChild(quitLabel)
        
        
        restartLabel.text = "Try Again"
        restartLabel.fontSize = 30
        restartLabel.fontColor = SKColor.green
        restartLabel.zPosition = 1
        restartLabel.position = CGPoint(x: self.size.width/2, y: self.size.height * 0.25)
        self.addChild(restartLabel)
        
        
        let scoreLabel = SKLabelNode(fontNamed: "SPACEBOY")
        scoreLabel.text = "Score: \(gameScore)"
        scoreLabel.fontSize = 30
        scoreLabel.fontColor = SKColor.green
        scoreLabel.position = CGPoint(x: self.size.width/2, y: self.size.height * 0.65)
        scoreLabel.zPosition = 5
        
        self.addChild(scoreLabel)
        
        
        let redEnemy = SKSpriteNode(imageNamed: "redEnemy")
        let greenEnemy = SKSpriteNode(imageNamed: "blueEnemy")
        let blueEnemy = SKSpriteNode(imageNamed: "greenEnemy")
        
        
        redEnemy.position = CGPoint(x: self.frame.width/2, y: self.frame.height / 2)
        greenEnemy.position = CGPoint(x: redEnemy.position.x - 50, y: self.frame.height / 2)
        blueEnemy.position = CGPoint(x: redEnemy.position.x + 50, y: self.frame.height / 2)
        
        redEnemy.setScale(0.5)
        redEnemy.zPosition = 2
        
        greenEnemy.setScale(0.5)
        greenEnemy.zPosition = 2
        
        blueEnemy.setScale(0.5)
        blueEnemy.zPosition = 2
        
        
       self.addChild(redEnemy)
        self.addChild(blueEnemy)
        self.addChild(greenEnemy)
        
        
        
        
        let fadeInAction = SKAction.fadeIn(withDuration: 0.5)
        restartLabel.run(fadeInAction)
        
        let scaleInAction = SKAction.scale(to: 1.2, duration: 1)
        let scaleOutAction = SKAction.scale(to: 1.1, duration: 1)
        let scaleSequence = SKAction.sequence([scaleInAction,scaleOutAction])
        
        restartLabel.run(SKAction.repeatForever(scaleSequence))
        
        
        let defaults = UserDefaults()
        var highScoreNumber = defaults.integer(forKey: "highScoreSaved")
        
        if gameScore > highScoreNumber{
            
            
            let newHighScore = SKLabelNode(fontNamed: "SPACEBOY")
            newHighScore.text = "New High Score!"
            newHighScore.fontSize = 20
            newHighScore.fontColor =  SKColor(red: 191/255, green: 25/255, blue: 0/255, alpha: 1)
            newHighScore.zPosition = 1
            newHighScore.position = CGPoint(x: self.size.width/2, y: self.size.height * 0.40)
            self.addChild(newHighScore)
            
            highScoreNumber = gameScore
            defaults.set(highScoreNumber, forKey: "highScoreSaved")
            
        }
        
        let highScoreLabel = SKLabelNode(fontNamed: "SPACEBOY")
        highScoreLabel.text = "High Score: \(highScoreNumber)"
        highScoreLabel.fontSize = 20
        highScoreLabel.fontColor =  SKColor(red: 191/255, green: 25/255, blue: 0/255, alpha: 1)
        highScoreLabel.zPosition = 1
        highScoreLabel.position = CGPoint(x: self.size.width/2, y: self.size.height * 0.60)
        self.addChild(highScoreLabel)
        
        
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    
        for touch:AnyObject in touches {
            
            let pointOfTouch = touch.location(in: self)
            
            if restartLabel.contains(pointOfTouch){
                manaRechargeValue = 0
                gameScore = 0
                let sceneToMoveTo = GameScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let myTransition = SKTransition.fade(withDuration: 0.5)
                self.view!.presentScene(sceneToMoveTo, transition: myTransition)
                
            }
            else if quitLabel.contains(pointOfTouch){
                manaRechargeValue = 0
                gameScore = 0
                let sceneToMoveTo = HomeScene(size: self.size)
                sceneToMoveTo.scaleMode = self.scaleMode
                let myTransition = SKTransition.fade(withDuration: 0.5)
                self.view!.presentScene(sceneToMoveTo, transition: myTransition)
                
                
                
            }
            
            
        }
        
        
        
    }
    
}
